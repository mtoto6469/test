<?php

namespace backend\controllers;

use Yii;
use common\models\Pages;
use common\models\PagesSearch;
//use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\components\Controller;


/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new PagesSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionIndex($params=null){
        $model=new Pages();
        if ($model->load(Yii::$app->request->post() )){
//            print_r($model);exit();
            if (!$model->validate()){print_r($model);exit;}

            if ($model->labelTitle){
                $titleModel=$this->findModel(Pages::ITEM_LABEL_TITLE);
                $titleModel->body=$model->labelTitle;
                $titleModel->save();
            }

            if ($model->en_labelTitle){
                $enTitleModel=$this->findModel(Pages::ITEM_EN_LABEL_TITLE);
                $enTitleModel->body=$model->en_labelTitle;
                $enTitleModel->save();
            }


            if ($model->seo_title){
                $seoTitleModel=$this->findModel(Pages::ITEM_SEO_TITLE);
                $seoTitleModel->body=$model->seo_title;
                $seoTitleModel->save();
            }

            if ($model->en_seo_title){
                $enSeoTitleModel=$this->findModel(Pages::ITEM_EN_SEO_TITLE);
                $enSeoTitleModel->body=$model->en_seo_title;
                $enSeoTitleModel->save();
            }

           if ($model->phone){
               $phoneModel=$this->findModel(Pages::ITEM_TELEPHONE);
               $phoneModel->body=$model->phone;
               $phoneModel->save();
           }

//            $faxModel=$this->findModel(Pages::ITEM_FAX);
//            $faxModel->body=$model->fax;
//            $faxModel->save();

           if ($model->email){
               $emailModel=$this->findModel(Pages::ITEM_EMAIL);
               $emailModel->body=$model->email;
               $emailModel->save();
           }

            if ($model->description){
                $descriptionModel=$this->findModel(Pages::ITEM_DESCRIPTION);
                $descriptionModel->body=$model->description;
                $descriptionModel->save();
            }

           if ($model->en_description){
               $enDescriptionModel=$this->findModel(Pages::ITEM_EN_DESCRIPTION);
               $enDescriptionModel->body=$model->en_description;
               $enDescriptionModel->save();
           }

           if ($model->seo_description){
               $seoDescriptionModel=$this->findModel(Pages::ITEM_SEO_DESCRIPTION);
               $seoDescriptionModel->body=$model->seo_description;
               $seoDescriptionModel->save();
           }

            if ($model->en_seo_description){
                $enSeoDescriptionModel=$this->findModel(Pages::ITEM_EN_SEO_DESCRIPTION);
                $enSeoDescriptionModel->body=$model->en_seo_description;
                $enSeoDescriptionModel->save();
            }

           if ($model->address){
               $addressModel=$this->findModel(Pages::ITEM_ADDRESS);
               $addressModel->body=$model->address;
               $addressModel->save();
           }

           if ($model->en_address){
               $enAddressModel=$this->findModel(Pages::ITEM_EN_ADDRESS);
               $enAddressModel->body=$model->en_address;
               $enAddressModel->save();
           }

            if ($model->telegram){
                $telegramModel=$this->findModel(Pages::ITEM_TELEGRAM);
                $telegramModel->body=$model->telegram;
                $telegramModel->save();
            }

            if ($model->instagram){
                $instagramModel=$this->findModel(Pages::ITEM_INSTEGERAM);
                $instagramModel->body=$model->instagram;
                $instagramModel->save();
            }

           if ($model->about){
               $aboutModel=$this->findModel(Pages::ITEM_ABOUT);
               $aboutModel->body=$model->about;
               $aboutModel->save();
           }

           if ($model->en_about){
               $enAboutModel=$this->findModel(Pages::ITEM_EN_ABOUT);
               $enAboutModel->body=$model->en_about;
               $enAboutModel->save();
           }

            if ($model->rule){
                $ruleModel=$this->findModel(Pages::ITEM_RULE);
                $ruleModel->body=$model->rule;
                $ruleModel->save();
            }

           if ($model->en_rule){
               $enRuleModel=$this->findModel(Pages::ITEM_EN_RULE);
               $enRuleModel->body=$model->en_rule;
               $enRuleModel->save();
           }

           if ($model->key_word){
               $seoKeyWordModel=$this->findModel(Pages::ITEM_KEYWORD);
               $seoKeyWordModel->body=$model->key_word;
               $seoKeyWordModel->save();
           }

            if ($model->en_key_word){
                $enSeoKeyWordModel=$this->findModel(Pages::ITEM_EN_KEYWORD);
                $enSeoKeyWordModel->body=$model->en_key_word;
                $enSeoKeyWordModel->save();
            }

            if ($model->strategic_program){
                $strategicProgramModel=$this->findModel(Pages::ITEM_STRATEGIC_PROGRAM);
                $strategicProgramModel->body=$model->strategic_program;
                $strategicProgramModel->save();
            }

            if ($model->en_strategic_program){
                $enStrategicProgramModel=$this->findModel(Pages::ITEM_EN_STRATEGIC_PROGRAM);
                $enStrategicProgramModel->body=$model->en_strategic_program;
                $enStrategicProgramModel->save();
            }

            $logo = UploadedFile::getInstance($model, 'logo');
            $enLogo = UploadedFile::getInstance($model, 'en_logo');
            $chart = UploadedFile::getInstance($model, 'chart');
            $enChart = UploadedFile::getInstance($model, 'en_chart');

            if ($logo){
                $image=Yii::$app->main->imageUpload($logo,'pages',null,$path='pages');
                $logoModel=$this->findModel(Pages::ITEM_LOGO);
                $logoModel->body=$image['image_id'];
                $logoModel->save();
            }
            if ($enLogo){
                $logoEN=Yii::$app->main->imageUpload($enLogo,'pages',null,$path='pages');
                    $enLogoModel=$this->findModel(Pages::ITEM_EN_LOGO);
                    $enLogoModel->body=$logoEN['image_id'];
                    $enLogoModel->save();
            }
            if ($chart){
                $chartFA=Yii::$app->main->imageUpload($chart,'pages',null,$path='pages');
                $chartModel=$this->findModel(Pages::ITEM_CHART);
                $chartModel->body=$chartFA['image_id'];
                $chartModel->save();
            }
            if ($enChart){
                $chartEN=Yii::$app->main->imageUpload($enChart,'pages',null,$path='pages');
                $enChartModel=$this->findModel(Pages::ITEM_EN_CHART);
                $enChartModel->body=$chartEN['image_id'];
                $enChartModel->save();
            }

//            if ($logo && $image_id = $model->uploadOne($logo)) {
//                $logoModel = $this->findModel(Setting::ITEM_LOGO);
//                $logoModel->value = (string)$image_id;
//                $logoModel->save();
//            }

            return $this->redirect(['pages/','params'=>$params]);
        }
//          return $this->redirect(['/pages']);
        $pages=Pages::find()->all();
        foreach ($pages as $page){
            $values[$page->key]=$page->body;
        }

        $model->logo=$values[Pages::ITEM_LOGO];
        $model->en_logo=$values[Pages::ITEM_EN_LOGO];
        $model->labelTitle=$values[Pages::ITEM_LABEL_TITLE];
        $model->en_labelTitle=$values[Pages::ITEM_EN_LABEL_TITLE];
        $model->seo_title=$values[Pages::ITEM_SEO_TITLE];
        $model->en_seo_title=$values[Pages::ITEM_EN_SEO_TITLE];
        $model->phone=$values[Pages::ITEM_TELEPHONE];
        $model->email=$values[Pages::ITEM_EMAIL];
        $model->description=$values[Pages::ITEM_DESCRIPTION];
        $model->en_description=$values[Pages::ITEM_EN_DESCRIPTION];
        $model->seo_description=$values[Pages::ITEM_SEO_DESCRIPTION];
        $model->en_seo_description=$values[Pages::ITEM_EN_SEO_DESCRIPTION];
        $model->address=$values[Pages::ITEM_ADDRESS];
        $model->en_address=$values[Pages::ITEM_EN_ADDRESS];
        $model->telegram=$values[Pages::ITEM_TELEGRAM];
        $model->instagram=$values[Pages::ITEM_INSTEGERAM];
        $model->about=$values[Pages::ITEM_ABOUT];
        $model->en_about=$values[Pages::ITEM_EN_ABOUT];
        $model->rule=$values[Pages::ITEM_RULE];
        $model->en_rule=$values[Pages::ITEM_EN_RULE];
        $model->key_word=$values[Pages::ITEM_KEYWORD];
        $model->en_key_word=$values[Pages::ITEM_EN_KEYWORD];
        $model->strategic_program=$values[Pages::ITEM_STRATEGIC_PROGRAM];
        $model->en_strategic_program=$values[Pages::ITEM_EN_STRATEGIC_PROGRAM];
        $model->chart=$values[Pages::ITEM_CHART];
        $model->en_chart=$values[Pages::ITEM_EN_CHART];
//        $model->fax=$values[Pages::ITEM_FAX];
//print_r($model);exit();
        return $this->render('index',[
            'model'=>$model,
            'records'=>$values,
            'params'=>$params,
        ]);

    }
    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
