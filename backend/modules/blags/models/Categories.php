<?php

namespace backend\modules\blags\models;

use common\models\User;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tbl_categories".
 *
 * @property int $cat_id
 * @property int $cat_user_id
 * @property int|null $cat_parent_id
 * @property string $cat_type
 * @property string $cat_title
 * @property string|null $cat_descriptions
 * @property string $cat_en_title
 * @property string|null $cat_en_descriptions
 * @property int|null $cat_image_id
 * @property int $cat_status
 * @property string $slug
 * @property int $cat_created_at
 * @property int $cat_updated_at
 * @property string $image
 *
 * @property User $catUser
 */
class Categories extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_categories';
    }
//    public function behaviors()
//    {
//        return[
//            'class'=>SluggableBehavior::className(),
//            'attribute'=>['cat_id'.'cat_en_title'],
//            'ensureUnique'=>true
//        ];
//
//    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['cat_user_id','default','value'=>Yii::$app->user->getId(),'on'=>'create'],
            [['cat_user_id', 'cat_title'], 'required'],
            [['cat_en_title','cat_en_descriptions'],'string'],
            [['cat_user_id', 'cat_parent_id', 'cat_image_id', 'cat_status', 'cat_created_at', 'cat_updated_at'], 'integer'],
            [['cat_type'], 'string', 'max' => 1],
            ['cat_status','default','value'=>'1'],
            [['cat_type'],'string'],
            [['slug'],'string'],
            [['slug'],'unique'],
            [['cat_type'],'default','value'=>'2'],
            ['cat_created_at','default','value'=>time()],
            ['cat_updated_at','default','value'=>time()],
//            ['cat_user_id','default','value'=>Yii::$app->user->getId()],
//            ['cat_user_id','default','value'=>Yii::$app->user->getId()],
            [['cat_title', 'cat_descriptions',], 'string', 'max' => 255],
            ['image','file','skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1],
            [['cat_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['cat_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cat_id' => Yii::t('app', 'Cat ID'),
            'cat_user_id' => Yii::t('app', 'Cat User ID'),
            'cat_parent_id' => Yii::t('app', 'Cat Parent ID'),
            'cat_type' => Yii::t('app', 'Cat Type'),
            'cat_title' => Yii::t('app', 'Cat Title'),
            'cat_en_title' => Yii::t('app', 'Cat Title'),
            'cat_descriptions' => Yii::t('app', 'Cat En Descriptions'),
            'cat_en_descriptions' => Yii::t('app', 'Cat En Descriptions'),
            'cat_image' => Yii::t('app', 'Cat Image'),
            'image' => Yii::t('app', 'Image'),
            'cat_status' => Yii::t('app', 'Cat Status'),
            'cat_created_at' => Yii::t('app', 'Cat Created At'),
            'cat_updated_at' => Yii::t('app', 'Cat Updated At'),
        ];
    }

    /**
     * Gets query for [[CatUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatUser()
    {
        return $this->hasOne(User::className(), ['id' => 'cat_user_id']);
    }
    public static function getCountCategory($item=null)
    {
        return self::find()->where(['cat_type'=>$item])
            ->andWhere(['cat_status'=>'1'])//active
            ->count();
    }

    public static function getCategories($item=null){
        return self::find()
            ->where(['cat_status'=>'1'])
            ->andWhere(['cat_type'=>$item])
            ->select(['cat_title','cat_id'])
            ->indexBy('cat_id')
            ->all();
    }
    public static function getSubCategories($item=null,$id=null)
    {
        if (self::find()
                ->where(['cat_type'=>$item])
                ->andWhere(['cat_parent_id'=>$id])
                ->count() > 0){
            return false;
        }
        return true;
    }

    public static function getDeleteAllCategories($item=null)
    {
        $categories=self::find()
            ->where(['cat_parent_id'=>$item])
            ->all();
        foreach ($categories as $category ){
            $category['cat_status']='2';
            if (!$category->update()){return false;}
            Blags::getDeleteBlag($category['cat_id']);
        }
        return true;
    }
}
