<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\Blags */

$this->title = $model->blg_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    img{
        width: 70px;
        height: 70px;
    }
</style>
<div class="blags-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->blg_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->blg_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'blg_id',
            'blgUser.username',
            'blgCategory.cat_title',
            'blg_title',
            'blg_body:ntext',
            'blg_descriptions',
            'blg_laid',
            [
                'attribute'=>'blg_image_id',
                'format' => 'image',
                'value'=>function($model){
                    return Yii::$app->main->showImage($model->blg_image_id, 'pages');
                },
                'label'=> 'عکس',
            ],
            'blg_keyword',
            'blg_meta_descriptions',
            'blg_meta_title',
            'blg_link',
            'slug',
            'blg_writer',
            'blg_view_count',
            'blg_like_count',
            'blg_comment_count',
            'blg_status',
            'blg_created_at',
            'blg_updated_at',
        ],
    ]) ?>

</div>
