<?php

use backend\modules\blags\models\Categories;
use common\models\Files;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use moonland\tinymce\TinyMCE;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\Blags */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blags-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <?= $form->field($model, 'image')->fileInput() ?>
        <?php if (!$model->isNewRecord){
            $images=Files::findOne(['img_id'=>$model->blg_image_id]) ?>
            <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/blogs/<?= $images->img_image ?>" width="70" height="70">
        <?php }else{?>
            <img id="aImgShow" class="carImage" height="70px" width="70px">
        <?php }  ?>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <?= $form->field($model, 'blg_category_id')->dropDownList(['prompt'=>'',Categories::getCategories()]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList(['2'=>Yii::t('app','article')],[
                //        'class'=>'add-in1',
                'prompt' => '',
                'onchange' => '$.post("' . Yii::$app->urlManager->createUrl('blags/categories/cat?item=') . '"+$(this).val(),function(data)
    {$("select#blags-blg_category_id"). html(data);
    });']) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-6"></div>
        <div class="col-md-6"><?= $form->field($model, 'blg_title')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12"><?= $form->field($model, 'blg_body')->widget(TinyMCE::className(),[
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls'=>false
            ]
        ]); ?> </div>
    <div class="col-md-12"> <?= $form->field($model, 'blg_keyword')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12"><?= $form->field($model, 'blg_meta_descriptions')->textInput(['rows' => 3]) ?></div>
    <div class="col-md-12"> <?= $form->field($model, 'blg_meta_title')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12">
        <div class="col-md-6"><?php $form->field($model, 'blg_link')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'blg_writer')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12"><?= $form->field($model, 'blg_status')->checkbox() ?></div>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>





    <?php ActiveForm::end(); ?>

</div>
