<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\BlagsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blags-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'blg_id') ?>

    <?= $form->field($model, 'blg_user_id') ?>

    <?= $form->field($model, 'blg_category_id') ?>

    <?= $form->field($model, 'blg_title') ?>

    <?= $form->field($model, 'blg_body') ?>

    <?php // echo $form->field($model, 'blg_descriptions') ?>

    <?php // echo $form->field($model, 'blg_laid') ?>

    <?php // echo $form->field($model, 'blg_image') ?>

    <?php // echo $form->field($model, 'blg_keyword') ?>

    <?php // echo $form->field($model, 'blg_meta_descriptions') ?>

    <?php // echo $form->field($model, 'blg_meta_title') ?>

    <?php // echo $form->field($model, 'blg_link') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'blg_writer') ?>

    <?php // echo $form->field($model, 'blg_view_count') ?>

    <?php // echo $form->field($model, 'blg_like_count') ?>

    <?php // echo $form->field($model, 'blg_comment_count') ?>

    <?php // echo $form->field($model, 'blg_status') ?>

    <?php // echo $form->field($model, 'blg_created_at') ?>

    <?php // echo $form->field($model, 'blg_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
