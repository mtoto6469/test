<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\Blags */

$this->title = Yii::t('app', 'Update Blags: {name}', [
    'name' => $model->blg_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->blg_id, 'url' => ['view', 'id' => $model->blg_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="blags-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
