<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\blags\models\BlagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blags');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    img{
        width: 70px;
        height: 70px;
    }
</style>
<div class="blags-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Blags'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'blg_id',
            'blgUser.username',
            'blgCategory.cat_title',
            'blg_title',
//            'blg_body:ntext',
            //'blg_descriptions',
            'blg_laid',

//            [
//                    'attribute'=>'blg_laid',
//                    'filter' => \common\models\Blags::find()->select(['blg_laid', 'blg_id'])->indexBy('blg_id')->column()
//            ],
            [
                'attribute'=>'blg_image_id',
                'format' => 'image',
                'value'=>function($model){
                    return Yii::$app->main->showImage($model->blg_image_id, 'pages');
                },
                'label'=> 'عکس',
            ],
            //'blg_image',
            //'blg_keyword',
            //'blg_meta_descriptions',
            //'blg_meta_title',
            //'blg_link',
            //'slug',
            //'blg_writer',
            //'blg_view_count',
            //'blg_like_count',
            //'blg_comment_count',
            //'blg_status',
            //'blg_created_at',
            //'blg_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
