<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\Blags */

$this->title = Yii::t('app', 'Create Blags');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blags-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
