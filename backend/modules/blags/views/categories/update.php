<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\Categories */

$this->title = Yii::t('app', 'Update Categories: {name}', [
    'name' => $model->cat_title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cat_id, 'url' => ['view', 'id' => $model->cat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="categories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
