<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\CategoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'cat_id') ?>

    <?= $form->field($model, 'cat_user_id') ?>

    <?= $form->field($model, 'cat_parent_id') ?>

    <?= $form->field($model, 'cat_type') ?>

    <?= $form->field($model, 'cat_title') ?>

    <?php // echo $form->field($model, 'cat_descriptions') ?>

    <?php // echo $form->field($model, 'cat_image') ?>

    <?php // echo $form->field($model, 'cat_status') ?>

    <?php // echo $form->field($model, 'cat_created_at') ?>

    <?php // echo $form->field($model, 'cat_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
