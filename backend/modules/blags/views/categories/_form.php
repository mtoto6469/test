<?php


use backend\modules\blags\models\Categories;
use common\models\Files;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\Categories */
///* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>
    <input type="hidden" value="2" name="cat_type">
<div class="col-md-12">
    <div class="col-md-6">
        <?php $form->field($model, 'cat_parent_id')->dropDownList(['prompt'=>'',Categories::getCategories()]) ?>
    </div>
    <div class="col-md-6">
        <?php $form->field($model, 'cat_type')->dropDownList(['2'=>Yii::t('app','article')],[
//        'class'=>'add-in1',
            'prompt' => '',
            'onchange' => '$.post("' . Yii::$app->urlManager->createUrl('blags/categories/list?item=') . '"+$(this).val(),function(data)
        {$("select#categories-cat_parent_id"). html(data);
        });']) ?>
    </div>

</div>
    <div class="col-md-12">
            <?= $form->field($model, 'image')->fileInput() ?>
            <?php if (!$model->isNewRecord){
                $images=Files::findOne(['img_id'=>$model->cat_image_id]) ?>
                <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/blogs/<?= $images->img_image ?>" width="70" height="70">
            <?php }else{?>
                <img id="aImgShow" class="carImage" height="70px" width="70px">
            <?php }  ?>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><?= $form->field($model, 'cat_title')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'cat_en_title')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12"><?= $form->field($model, 'cat_descriptions')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12"><?= $form->field($model, 'cat_en_descriptions')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12"><?= $form->field($model, 'cat_status')->checkbox() ?></div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
