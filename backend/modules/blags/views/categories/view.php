<?php

use common\component\Images;
use common\models\Categories;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\blags\models\Categories */

$this->title = $model->cat_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>img {
        width: 50px;
        height: 50px
    }</style>
<div class="categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php  if(\Yii::$app->user->can('update_category')){
        echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->cat_id], ['class' => 'btn btn-primary']);
            }?>
        <?php  if(\Yii::$app->user->can('delete_category')) {
            echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->cat_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
        } ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'cat_id',
            'catUser.username',
//            [
//                'attribute' => 'cat_parent_id',
//                'value' => function ($model) {
//                    $patent = Categories::findOne($model->cat_parent_id);
//                    if ($patent) {
//                        return $patent->cat_title;
//                    } else {
//                        return null;
//                    }
//                },
//                'label' => 'دسته والد',
//
//            ],
//            [
//                'attribute' => 'cat_type',
//                'value' => function ($model) {
//
//                    if ($model->cat_type=='1') {
//                        return Yii::t('app','News');
//                    } elseif($model->cat_type=='2') {
//                        return Yii::t('app','Posts');
//                    }
//                },
//                'label' => 'نوع',
//
//            ],
            'cat_title',
            'cat_descriptions',
            [
                'attribute'=>'cat_image_id',
                'format' => 'image',
                'value'=>function($model){
                    return Yii::$app->main->showImage($model->cat_image_id, 'pages');
                },
                'label'=> 'عکس',
            ],
            [
                'attribute' => 'cat_status',
                'value' => function ($model) {

                    if ($model->cat_type=='0') {
                        return Yii::t('app','Activeََ');
                    } elseif($model->cat_type=='1') {
                        return Yii::t('app','Not Active');
                    }
                },
                'label' => 'وضعیت',

            ],
//            'cat_created_at:date',
//            'cat_updated_at:date',
        ],
    ]) ?>

</div>
