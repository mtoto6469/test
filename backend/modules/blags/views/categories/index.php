<?php

use backend\modules\blags\models\Categories;
use common\component\Images;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\blags\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    img{
        width:70px;
        height: 70px;
    }
</style>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php  if(\Yii::$app->user->can('add_category')){

            echo Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']);
        } ?>

    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'cat_image_id',
                'format' => 'image',
                'value'=>function($model){
                    return Yii::$app->main->showImage($model->cat_image_id, 'pages');
                },
                'label' => 'عکس',
            ],
//            [
//                'attribute' => 'cat_parent_id',
//                'value' => function ($model) {
//                    $patent = Categories::findOne($model->cat_parent_id);
//                    if ($patent) {
//                        return $patent->cat_title;
//                    } else {
//                        return null;
//                    }
//                },
//                'label' => 'دسته والد',
//
//            ],
//            [
//                'attribute' => 'cat_type',
//                'value' => function ($model) {
//                    if ($model->cat_type == '1') {
//                        return Yii::t('app', 'News');
//                    } elseif ($model->cat_type == '2') {
//                        return Yii::t('app', 'Posts');
//                    }
//                },
//                'label' => 'نوع',
//
//            ],
            'cat_title',
            [
                'attribute' => 'cat_status',
                'value' => function ($model) {

                    if ($model->cat_status == '1') {
                        return Yii::t('app', 'Activeََ');
                    } elseif ($model->cat_status == '0') {
                        return Yii::t('app', 'Not Active');
                    }
                },
                'label' => 'وضعیت',

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{status} {view} {update} {delete}',
                'visibleButtons' => [
                    'update' => function ($model) {
                        return \Yii::$app->user->can('update_category', ['post' => $model]);
                    },
                    'delete' => function ($model) {
                        return \Yii::$app->user->can('delete_category', ['post' => $model]);
                    },
                    'view' => function ($model) {
                        return \Yii::$app->user->can('admin_category', ['post' => $model]);
                    },


//                    'status' => function ($url, $model) {
//                        if ($model->cat_status == '1') {
//                            return \Yii::$app->user->can('update_category', Html::a(
//                                '<span class="glyphicon glyphicon-ok-circle"></span>',
//                                $url,
//                                [
//                                    'title' => 'status',
//                                    'data-pjax' => '0',
//                                ]
//                            ));
//                        } elseif ($model->cat_status == '0') {
//                            return \Yii::$app->user->can('update_category', Html::a(
//                                '<span class="glyphicon glyphicon-folder-close"></span>',
//                                $url,
//                                [
//                                    'title' => 'status',
//                                    'data-pjax' => '1',
//                                ]
//                            ));
//
//                        }
//                    }
                ],

                'buttons' => [
                    'status' => function ($url,$model) {
                         if ($model->cat_status == '1'){
                             $a =\Yii::$app->user->can('delete_category',Html::a(
                                 '<span class="glyphicon glyphicon-ok-circle"></span>',
                                 $url,
                                 [
                                     'title' => 'status',
                                     'data-pjax' => '0',
                                 ]
                             ));
                         }elseif ($model->cat_status == '0'){
                             $a= \Yii::$app->user->can('delete_category',Html::a(
                                 '<span class="glyphicon glyphicon-folder-close"></span>',
                                 $url,
                                 [
                                     'title' => 'status',
                                     'data-pjax' => '1',
                                 ]
                             ));
                         }
//                         return $a;

                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
