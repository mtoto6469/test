<?php

namespace backend\modules\blags\controllers;

use Yii;
use backend\modules\blags\models\Blags;
use backend\modules\blags\models\BlagsSearch;
use yii\filters\AccessControl;
//use yii\web\Controller;
use common\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BlagsController implements the CRUD actions for Blags model.
 */
class BlagsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'signup'],
                'rules' => [


                    [
                        'actions' => ['index', 'create', 'update','view','delete'],
                        'allow' => true,
                        'roles' => ['admin', 'admin'],
                    ],
                    [
                        'actions' => ['index', 'create', 'update','view','delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blags models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlagsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blags model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blags model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($lng=null)
    {
        $model = new Blags();
        $model->scenario='create';
        if ($model->load(Yii::$app->request->post())) {
            $model->blg_laid=$lng;
            $image=UploadedFile::getInstance($model,'image');
            if ($image){
                $imageX=Yii::$app->main->ImageUpload($image,'service',null,'pages');
                $model->blg_image_id=$imageX['image_id'];
            }
            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->blg_id]);
            }else{
                var_dump($model->errors);exit;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Blags model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario='update';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->blg_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Blags model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->blg_status='2';
        $model->update();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blags model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blags the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blags::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
