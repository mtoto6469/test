<?php

namespace backend\modules\blags\controllers;

use backend\modules\blags\models\Blags;
use common\component\Func;
//use common\component\Images;
use Yii;
use backend\modules\blags\models\Categories;
use backend\modules\blags\models\CategoriesSearch;
use yii\filters\AccessControl;
//use yii\web\Controller;
use common\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'signup'],
                'rules' => [


                    [
                        'actions' => ['index', 'create', 'update','view','delete'],
                        'allow' => true,
                        'roles' => ['admin', 'admin'],
                    ],
                    [
                        'actions' => ['index', 'create', 'update','view','delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionList($item){
        $countType=Categories::getCountCategory($item);
        $types =Categories::getCategories($item);
        if ($countType > 0)
        {
            echo "<option value='0'>".Yii::t('app','Enter Category Parent')."</option>";
            foreach ($types as $type){
                    echo "<option value='".$type->cat_id."'>".$type->cat_title."</option>";
            }
        }else{
            echo "<option></option>";
        }
    }
    public function actionCat($item){
        $countType=Categories::getCountCategory($item);
        $types =Categories::getCategories($item);
        if ($countType > 0)
        {
            echo "<option>".Yii::t('app','Enter Category Parent')."</option>";
            foreach ($types as $type){
                if (Categories::getSubCategories($item,$type->cat_id)){
                    echo "<option value='".$type->cat_id."'>".$type->cat_title."</option>";
                }
            }
        }else{
            echo "<option></option>";
        }
    }


    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {


            $model = new Categories();

            $model->scenario = 'create';
            if ($model->load(Yii::$app->request->post())) {
//
                $image=UploadedFile::getInstance($model,'image');
                if ($image){
                    $imageX=Yii::$app->main->ImageUpload($image,'service',null,'pages');
                    $model->cat_image_id=$imageX['image_id'];
                }
//                if($model){
//                    $imageX=Images::ImageUpload($image);
//                    if($imageX['result']==true){
//                        Yii::$app->session->setFlash('Success','Saved');
//                        $model->cat_image=$imageX['name'];
//                    }else{
//                        Yii::$app->session->setFlash('Error','Error to save');
//                    }
//
//                }

                $model->slug=$model->cat_en_title;
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->cat_id]);
                } else {
                    var_dump($model->errors);
                    exit;
                }
            }
            return $this->render('create', [
                'model' => $model,
            ]);



    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $image=UploadedFile::getInstance($model,'image');
            if ($image){
                $imageX=Yii::$app->main->ImageUpload($image,'service',null,'pages');
                $model->cat_image_id=$imageX['image_id'];
            }

            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->cat_id]);
            }else{
                var_dump($model->errors);exit;
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    //delete: param $cat_status='2'
    public function actionDelete($id)
    {
       $model= $this->findModel($id);
       Blags::getDeleteBlag($id);
       $model->cat_status='2';
       if ($model->update()){
           return $this->redirect(['index']);
       }
       return false;

    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
