<?php

namespace backend\modules\products\controllers;

use backend\modules\products\models\Products;
use common\component\Images;
use Yii;
use backend\modules\products\models\Brands;
use backend\modules\products\models\BrandsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BrandsController implements the CRUD actions for Brands model.
 */
class BrandsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brands models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrandsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brands model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brands model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Brands();

        $model->scenario='create';
        if ($model->load(Yii::$app->request->post())) {
            $image=UploadedFile::getInstance($model,'image');
            if ($model){
                $imageX=Images::ImageUpload($image);
                if ($imageX['result']==true){
                    $model->brn_image=$imageX['name'];
                }else{
                    var_dump($model->errors);exit;
                }
            }
            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->brn_id]);
            }else{
                var_dump($model->errors);exit;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Brands model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario='update';

        if ($model->load(Yii::$app->request->post())) {
            $image=UploadedFile::getInstance($model,'image');
            if ($model){
                $imageX=Images::ImageUpload($image);
                if ($imageX['result']==true){
                    $model->brn_image=$imageX['name'];
                }
            }
            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->brn_id]);
            }else{
                var_dump($model->errors);exit;
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Brands model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        Products::getDeleteProduct($id);
        $model->brn_status='2';
        if ($model->update()){
            return $this->redirect(['index']);
        }
        return false;

    }

    /**
     * Finds the Brands model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brands the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brands::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
