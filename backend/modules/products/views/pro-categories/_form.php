<?php

use backend\modules\products\models\ProCategories;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\products\models\ProCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pro-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cat_parent_id')->textInput(['prompt'=>Yii::t('app','enter'),ProCategories::getProCategory()]) ?>

    <?= $form->field($model, 'cat_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cat_descriptions')->textInput(['maxlength' => true]) ?>
    <?php if($model->isNewRecord){ ?>
        <?= $form->field($model, 'image')->fileInput() ?>

    <?php } else{ ?>
        <?= $form->field($model,'image')->fileInput() ?>
        <div class="form-group">
            <img src="../../../../upload<?= $model->blg_image ?>" width="70"height="70" style="margin:0 10%;">
        </div>
    <?php  } ?>

    <?= $form->field($model, 'cat_status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
