<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\products\models\Products */

$this->title = $model->pro_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pro_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pro_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pro_id',
            'pro_user_id',
            'pro_category_id',
            'pro_brand_id',
            'pro_color_id',
            'pro_size_id',
            'pro_supplier_id',
            'pro_title',
            'pro_price',
            'pro_sale_off',
            'pro_body:ntext',
            'pro_descriptions',
            'pro_image',
            'pro_keyword',
            'pro_meta_descriptions',
            'pro_meta_title',
            'pro_link',
            'slug',
            'pro_writer',
            'pro_product_count',
            'pro_view_count',
            'pro_like_count',
            'pro_comment_count',
            'pro_status',
            'pro_begin_date_sale_off',
            'pro_end_date_sale_off',
            'pro_created_at',
            'pro_updated_at',
        ],
    ]) ?>

</div>
