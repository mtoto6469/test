<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\products\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pro_id') ?>

    <?= $form->field($model, 'pro_user_id') ?>

    <?= $form->field($model, 'pro_category_id') ?>

    <?= $form->field($model, 'pro_brand_id') ?>

    <?= $form->field($model, 'pro_color_id') ?>

    <?php // echo $form->field($model, 'pro_size_id') ?>

    <?php // echo $form->field($model, 'pro_supplier_id') ?>

    <?php // echo $form->field($model, 'pro_title') ?>

    <?php // echo $form->field($model, 'pro_price') ?>

    <?php // echo $form->field($model, 'pro_sale_off') ?>

    <?php // echo $form->field($model, 'pro_body') ?>

    <?php // echo $form->field($model, 'pro_descriptions') ?>

    <?php // echo $form->field($model, 'pro_image') ?>

    <?php // echo $form->field($model, 'pro_keyword') ?>

    <?php // echo $form->field($model, 'pro_meta_descriptions') ?>

    <?php // echo $form->field($model, 'pro_meta_title') ?>

    <?php // echo $form->field($model, 'pro_link') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'pro_writer') ?>

    <?php // echo $form->field($model, 'pro_product_count') ?>

    <?php // echo $form->field($model, 'pro_view_count') ?>

    <?php // echo $form->field($model, 'pro_like_count') ?>

    <?php // echo $form->field($model, 'pro_comment_count') ?>

    <?php // echo $form->field($model, 'pro_status') ?>

    <?php // echo $form->field($model, 'pro_begin_date_sale_off') ?>

    <?php // echo $form->field($model, 'pro_end_date_sale_off') ?>

    <?php // echo $form->field($model, 'pro_created_at') ?>

    <?php // echo $form->field($model, 'pro_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
