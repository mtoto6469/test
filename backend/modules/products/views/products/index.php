<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\products\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pro_id',
            'pro_user_id',
            'pro_category_id',
            'pro_brand_id',
            'pro_color_id',
            //'pro_size_id',
            //'pro_supplier_id',
            //'pro_title',
            //'pro_price',
            //'pro_sale_off',
            //'pro_body:ntext',
            //'pro_descriptions',
            //'pro_image',
            //'pro_keyword',
            //'pro_meta_descriptions',
            //'pro_meta_title',
            //'pro_link',
            //'slug',
            //'pro_writer',
            //'pro_product_count',
            //'pro_view_count',
            //'pro_like_count',
            //'pro_comment_count',
            //'pro_status',
            //'pro_begin_date_sale_off',
            //'pro_end_date_sale_off',
            //'pro_created_at',
            //'pro_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
