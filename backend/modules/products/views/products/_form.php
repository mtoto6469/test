<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\products\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pro_category_id')->textInput() ?>

    <?= $form->field($model, 'pro_brand_id')->textInput() ?>

    <?= $form->field($model, 'pro_color_id')->textInput() ?>

    <?= $form->field($model, 'pro_size_id')->textInput() ?>

    <?= $form->field($model, 'pro_supplier_id')->textInput() ?>

    <?= $form->field($model, 'pro_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pro_price')->textInput() ?>

    <?= $form->field($model, 'pro_sale_off')->textInput() ?>

    <?= $form->field($model, 'pro_body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pro_descriptions')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->checkbox()?>

    <?= $form->field($model, 'pro_keyword')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pro_meta_descriptions')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pro_meta_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pro_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pro_writer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pro_status')->checkbox() ?>

    <?= $form->field($model, 'pro_begin_date_sale_off')->textInput() ?>

    <?= $form->field($model, 'pro_end_date_sale_off')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
