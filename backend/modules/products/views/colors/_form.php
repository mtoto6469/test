<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\products\models\Colors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colors-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'col_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'col_descriptions')->textInput(['maxlength' => true]) ?>

    <?php if($model->isNewRecord){ ?>
        <?= $form->field($model, 'image')->fileInput() ?>

    <?php } else{ ?>
        <?= $form->field($model,'image')->fileInput() ?>
        <div class="form-group">
            <img src="../../../../upload<?= $model->col_image ?>" width="70"height="70" style="margin:0 10%;">
        </div>
    <?php  } ?>

    <?= $form->field($model, 'col_status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
