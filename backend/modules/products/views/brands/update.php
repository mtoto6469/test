<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\products\models\Brands */

$this->title = Yii::t('app', 'Update Brands: {name}', [
    'name' => $model->brn_title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->brn_id, 'url' => ['view', 'id' => $model->brn_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="brands-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
