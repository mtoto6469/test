<?php

namespace backend\modules\products\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "tbl_pro_categories".
 *
 * @property int $cat_id
 * @property int|null $cat_user_id
 * @property int|null $cat_parent_id
 * @property string|null $cat_title
 * @property string|null $cat_descriptions
 * @property string|null $cat_image
 * @property int $cat_status
 * @property int $cat_created_at
 * @property int $cat_updated_at
 *
 * @property User $catUser
 */
class ProCategories extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_pro_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_user_id', 'cat_parent_id', 'cat_created_at', 'cat_updated_at'], 'integer'],
            [['cat_created_at', 'cat_updated_at'], 'required'],
            [['cat_title', 'cat_descriptions', 'cat_image'], 'string', 'max' => 255],
            ['cat_status','string','max'=>1],
            ['cat_status','default','value'=>'1'],
            ['cat_user_id','default','value'=>Yii::$app->user->getId()],
            ['cat_created_at','default','value'=>time(),'on'=>'create'],
            ['cat_updated_at','default','value'=>time(),'on'=>'update'],
            ['image','file'],
            [['cat_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['cat_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cat_id' => Yii::t('app', 'Cat ID'),
            'cat_user_id' => Yii::t('app', 'Cat User ID'),
            'cat_parent_id' => Yii::t('app', 'Cat Parent ID'),
            'cat_title' => Yii::t('app', 'Cat Title'),
            'cat_descriptions' => Yii::t('app', 'Cat Descriptions'),
            'cat_image' => Yii::t('app', 'Cat Image'),
            'image' => Yii::t('app', 'Image'),
            'cat_status' => Yii::t('app', 'Cat Status'),
            'cat_created_at' => Yii::t('app', 'Cat Created At'),
            'cat_updated_at' => Yii::t('app', 'Cat Updated At'),
        ];
    }

    /**
     * Gets query for [[CatUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatUser()
    {
        return $this->hasOne(User::className(), ['id' => 'cat_user_id']);
    }
    public static function getCountProCategory()
    {
        return self::find()
            ->where(['cat_status'=>'1'])
            ->count();
    }
    public static function getProCategory()
    {
        return self::find()
            ->where(['cat_status'=>'1'])
            ->select(['cat_title','cat_id'])
            ->all();
    }
    public static function getDeleteAllProCategories($item)
    {
        $categories=self::find()
            ->where(['cat_parent_id'=>$item])
            ->all();
        foreach ($categories as $category){
            $category['cat_status']='2';
            if (!$category->update()){return false;}
            Products::getDeleteProduct('pro_category_id',$category['cat_id']);
        }
        return true;
    }
}
