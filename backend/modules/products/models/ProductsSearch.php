<?php

namespace backend\modules\products\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\products\models\Products;

/**
 * ProductsSearch represents the model behind the search form of `backend\modules\products\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pro_id', 'pro_user_id', 'pro_category_id', 'pro_brand_id', 'pro_color_id', 'pro_size_id', 'pro_supplier_id', 'pro_price', 'pro_sale_off', 'pro_product_count', 'pro_view_count', 'pro_like_count', 'pro_comment_count', 'pro_status', 'pro_begin_date_sale_off', 'pro_end_date_sale_off', 'pro_created_at', 'pro_updated_at'], 'integer'],
            [['pro_title', 'pro_body', 'pro_descriptions', 'pro_image', 'pro_keyword', 'pro_meta_descriptions', 'pro_meta_title', 'pro_link', 'slug', 'pro_writer'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pro_id' => $this->pro_id,
            'pro_user_id' => $this->pro_user_id,
            'pro_category_id' => $this->pro_category_id,
            'pro_brand_id' => $this->pro_brand_id,
            'pro_color_id' => $this->pro_color_id,
            'pro_size_id' => $this->pro_size_id,
            'pro_supplier_id' => $this->pro_supplier_id,
            'pro_price' => $this->pro_price,
            'pro_sale_off' => $this->pro_sale_off,
            'pro_product_count' => $this->pro_product_count,
            'pro_view_count' => $this->pro_view_count,
            'pro_like_count' => $this->pro_like_count,
            'pro_comment_count' => $this->pro_comment_count,
            'pro_status' => $this->pro_status,
            'pro_begin_date_sale_off' => $this->pro_begin_date_sale_off,
            'pro_end_date_sale_off' => $this->pro_end_date_sale_off,
            'pro_created_at' => $this->pro_created_at,
            'pro_updated_at' => $this->pro_updated_at,
        ]);

        $query->andFilterWhere(['like', 'pro_title', $this->pro_title])
            ->andFilterWhere(['like', 'pro_body', $this->pro_body])
            ->andFilterWhere(['like', 'pro_descriptions', $this->pro_descriptions])
            ->andFilterWhere(['like', 'pro_image', $this->pro_image])
            ->andFilterWhere(['like', 'pro_keyword', $this->pro_keyword])
            ->andFilterWhere(['like', 'pro_meta_descriptions', $this->pro_meta_descriptions])
            ->andFilterWhere(['like', 'pro_meta_title', $this->pro_meta_title])
            ->andFilterWhere(['like', 'pro_link', $this->pro_link])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'pro_writer', $this->pro_writer]);

        return $dataProvider;
    }
}
