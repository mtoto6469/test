<?php

namespace backend\modules\products\models;

use common\models\User;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tbl_products".
 *
 * @property int $pro_id
 * @property int|null $pro_user_id
 * @property int $pro_category_id
 * @property int $pro_brand_id
 * @property int $pro_color_id
 * @property int $pro_size_id
 * @property int $pro_supplier_id
 * @property string|null $pro_title
 * @property int|null $pro_price
 * @property int|null $pro_sale_off
 * @property string|null $pro_body
 * @property string|null $pro_descriptions
 * @property string|null $pro_image
 * @property string|null $pro_keyword
 * @property string|null $pro_meta_descriptions
 * @property string|null $pro_meta_title
 * @property string|null $pro_link
 * @property string|null $slug
 * @property string|null $pro_writer
 * @property int|null $pro_product_count
 * @property int|null $pro_view_count
 * @property int|null $pro_like_count
 * @property int|null $pro_comment_count
 * @property int $pro_status
 * @property int|null $pro_begin_date_sale_off
 * @property int|null $pro_end_date_sale_off
 * @property int $pro_created_at
 * @property int $pro_updated_at
 * @property string $image
 *
 * @property User $proUser
 */
class Products extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_products';
    }

    public function behaviors()
    {
        return[
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'pro_title',
                'ensureUnique'=>true
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pro_user_id', 'pro_category_id', 'pro_brand_id', 'pro_color_id', 'pro_size_id', 'pro_supplier_id', 'pro_price', 'pro_sale_off', 'pro_product_count', 'pro_view_count', 'pro_like_count', 'pro_comment_count', 'pro_begin_date_sale_off', 'pro_end_date_sale_off', 'pro_created_at', 'pro_updated_at'], 'integer'],
            [['pro_category_id', 'pro_brand_id', 'pro_color_id', 'pro_size_id', 'pro_supplier_id', 'pro_created_at', 'pro_updated_at'], 'required'],
            [['pro_body'], 'string'],
            [['pro_title', 'pro_descriptions', 'pro_image', 'pro_keyword', 'pro_meta_descriptions', 'pro_meta_title', 'pro_link', 'slug', 'pro_writer'], 'string', 'max' => 255],
            ['pro_user_id','default','value'=>Yii::$app->user->getId(),'on'=>'create'],
            ['pro_status','string','max'=>1],
            ['pro_status','default','value'=>'1'],
            ['pro_created_at','default','value'=>time(),'on'=>'create'],
            ['pro_updated_at','default','value'=>time(),'on'=>'update'],
            ['image','file'],
            [['pro_title'], 'unique'],
            [['slug'], 'unique'],
            [['pro_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['pro_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pro_id' => Yii::t('app', 'Pro ID'),
            'pro_user_id' => Yii::t('app', 'Pro User ID'),
            'pro_category_id' => Yii::t('app', 'Pro Category ID'),
            'pro_brand_id' => Yii::t('app', 'Pro Brand ID'),
            'pro_color_id' => Yii::t('app', 'Pro Color ID'),
            'pro_size_id' => Yii::t('app', 'Pro Size ID'),
            'pro_supplier_id' => Yii::t('app', 'Pro Supplier ID'),
            'pro_title' => Yii::t('app', 'Pro Title'),
            'pro_price' => Yii::t('app', 'Pro Price'),
            'pro_sale_off' => Yii::t('app', 'Pro Sale Off'),
            'pro_body' => Yii::t('app', 'Pro Body'),
            'pro_descriptions' => Yii::t('app', 'Pro Descriptions'),
            'pro_image' => Yii::t('app', 'Pro Image'),
            'image' => Yii::t('app', 'Image'),
            'pro_keyword' => Yii::t('app', 'Pro Keyword'),
            'pro_meta_descriptions' => Yii::t('app', 'Pro Meta Descriptions'),
            'pro_meta_title' => Yii::t('app', 'Pro Meta Title'),
            'pro_link' => Yii::t('app', 'Pro Link'),
            'slug' => Yii::t('app', 'Slug'),
            'pro_writer' => Yii::t('app', 'Pro Writer'),
            'pro_product_count' => Yii::t('app', 'Pro Product Count'),
            'pro_view_count' => Yii::t('app', 'Pro View Count'),
            'pro_like_count' => Yii::t('app', 'Pro Like Count'),
            'pro_comment_count' => Yii::t('app', 'Pro Comment Count'),
            'pro_status' => Yii::t('app', 'Pro Status'),
            'pro_begin_date_sale_off' => Yii::t('app', 'Pro Begin Date Sale Off'),
            'pro_end_date_sale_off' => Yii::t('app', 'Pro End Date Sale Off'),
            'pro_created_at' => Yii::t('app', 'Pro Created At'),
            'pro_updated_at' => Yii::t('app', 'Pro Updated At'),
        ];
    }

    /**
     * Gets query for [[ProUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProUser()
    {
        return $this->hasOne(User::className(), ['id' => 'pro_user_id']);
    }
    public static function getDeleteProduct($field,$item)
    {
      $products=self::find()
          ->where([$field=>$item])
          ->all();
      foreach ($products as $product)
      {
          $product['pro_status']='2';
          if (!$product->update()){return false;}
      }
      return true;
    }

}
