<?php

namespace backend\modules\products\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\products\models\ProCategories;

/**
 * ProCategoriesSearch represents the model behind the search form of `backend\modules\products\models\ProCategories`.
 */
class ProCategoriesSearch extends ProCategories
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'cat_user_id', 'cat_parent_id', 'cat_status', 'cat_created_at', 'cat_updated_at'], 'integer'],
            [['cat_title', 'cat_descriptions', 'cat_image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProCategories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cat_id' => $this->cat_id,
            'cat_user_id' => $this->cat_user_id,
            'cat_parent_id' => $this->cat_parent_id,
            'cat_status' => $this->cat_status,
            'cat_created_at' => $this->cat_created_at,
            'cat_updated_at' => $this->cat_updated_at,
        ]);

        $query->andFilterWhere(['like', 'cat_title', $this->cat_title])
            ->andFilterWhere(['like', 'cat_descriptions', $this->cat_descriptions])
            ->andFilterWhere(['like', 'cat_image', $this->cat_image]);

        return $dataProvider;
    }
}
