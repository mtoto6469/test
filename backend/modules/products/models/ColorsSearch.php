<?php

namespace backend\modules\products\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\products\models\Colors;

/**
 * ColorsSearch represents the model behind the search form of `backend\modules\products\models\Colors`.
 */
class ColorsSearch extends Colors
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['col_id', 'col_user_id', 'col_status', 'col_created_at', 'col_updated_at'], 'integer'],
            [['col_title', 'col_descriptions', 'col_image', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Colors::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'col_id' => $this->col_id,
            'col_user_id' => $this->col_user_id,
            'col_status' => $this->col_status,
            'col_created_at' => $this->col_created_at,
            'col_updated_at' => $this->col_updated_at,
        ]);

        $query->andFilterWhere(['like', 'col_title', $this->col_title])
            ->andFilterWhere(['like', 'col_descriptions', $this->col_descriptions])
            ->andFilterWhere(['like', 'col_image', $this->col_image])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
