<?php

namespace backend\modules\products\models;

use common\models\User;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tbl_brands".
 *
 * @property int $brn_id
 * @property int|null $brn_user_id
 * @property string|null $brn_title
 * @property string|null $brn_descriptions
 * @property string|null $brn_image
 * @property string $slug
 * @property int $brn_status
 * @property int $brn_created_at
 * @property int $brn_updated_at
 *
 * @property User $brnUser
 */
class Brands extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_brands';
    }
    public function behaviors()
    {
        return[
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'brn_title',
                'ensureUnique'=>true,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brn_user_id',  'brn_created_at', 'brn_updated_at'], 'integer'],
            [['slug', 'brn_created_at', 'brn_updated_at'], 'required'],
            [['brn_title', 'brn_descriptions', 'brn_image', 'slug'], 'string', 'max' => 255],
            ['brn_status','string','max'=>1],
            ['brn_status','default','value'=>'1'],
            ['brn_created_at','default','value'=>time(),'on'=>'create'],
            ['brn_updated_at','default','value'=>time(),'on'=>'update'],
            ['brn_user_id','default','value'=>Yii::$app->user->getId(),'on'=>'create'],
            ['image','file'],
            [['slug'], 'unique'],
            [['brn_title'], 'unique'],
            [['brn_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['brn_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'brn_id' => Yii::t('app', 'Brn ID'),
            'brn_user_id' => Yii::t('app', 'Brn User ID'),
            'brn_title' => Yii::t('app', 'Brn Title'),
            'brn_descriptions' => Yii::t('app', 'Brn Descriptions'),
            'brn_image' => Yii::t('app', 'Brn Image'),
            'image' => Yii::t('app', 'Image'),
            'slug' => Yii::t('app', 'Slug'),
            'brn_status' => Yii::t('app', 'Brn Status'),
            'brn_created_at' => Yii::t('app', 'Brn Created At'),
            'brn_updated_at' => Yii::t('app', 'Brn Updated At'),
        ];
    }

    /**
     * Gets query for [[BrnUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrnUser()
    {
        return $this->hasOne(User::className(), ['id' => 'brn_user_id']);
    }
    public function getBrands()
    {
        return self::findAll(['brn_status'=>'1']);
    }
}
