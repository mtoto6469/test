<?php

namespace backend\modules\products\models;

use common\models\User;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tbl_colors".
 *
 * @property int $col_id
 * @property int|null $col_user_id
 * @property string|null $col_title
 * @property string|null $col_descriptions
 * @property string|null $col_image
 * @property string $slug
 * @property int $col_status
 * @property int $col_created_at
 * @property int $col_updated_at
 *
 * @property User $colUser
 */
class Colors extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_colors';
    }
    public function behaviors()
    {
        return[
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'col_title',
                'ensureUnique'=>true
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['col_user_id', 'col_created_at', 'col_updated_at'], 'integer'],
            [['slug', 'col_created_at', 'col_updated_at'], 'required'],
            [['col_title', 'col_descriptions', 'col_image', 'slug'], 'string', 'max' => 255],
            ['col_user_id','default','value'=>Yii::$app->user->getId(),'no'=>'create'],
            ['col_status','string','max'=>1],
            ['col_status','default','value'=>'1'],
            ['col_created_at','default','value'=>time(),'on'=>'create'],
            ['col_updated_at','default','value'=>time(),'on'=>'update'],
            ['image','file'],
            [['slug'], 'unique'],
            [['col_title'], 'unique'],
            [['col_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['col_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'col_id' => Yii::t('app', 'Col ID'),
            'col_user_id' => Yii::t('app', 'Col User ID'),
            'col_title' => Yii::t('app', 'Col Title'),
            'col_descriptions' => Yii::t('app', 'Col Descriptions'),
            'col_image' => Yii::t('app', 'Col Image'),
            'image' => Yii::t('app', 'Image'),
            'slug' => Yii::t('app', 'Slug'),
            'col_status' => Yii::t('app', 'Col Status'),
            'col_created_at' => Yii::t('app', 'Col Created At'),
            'col_updated_at' => Yii::t('app', 'Col Updated At'),
        ];
    }

    /**
     * Gets query for [[ColUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColUser()
    {
        return $this->hasOne(User::className(), ['id' => 'col_user_id']);
    }
    public static function getColors()
    {
        return self::findAll(['col_status'=>'1']);
    }
}
