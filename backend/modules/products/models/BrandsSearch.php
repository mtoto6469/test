<?php

namespace backend\modules\products\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\products\models\Brands;

/**
 * BrandsSearch represents the model behind the search form of `backend\modules\products\models\Brands`.
 */
class BrandsSearch extends Brands
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brn_id', 'brn_user_id', 'brn_status', 'brn_created_at', 'brn_updated_at'], 'integer'],
            [['brn_title', 'brn_descriptions', 'brn_image', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Brands::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'brn_id' => $this->brn_id,
            'brn_user_id' => $this->brn_user_id,
            'brn_status' => $this->brn_status,
            'brn_created_at' => $this->brn_created_at,
            'brn_updated_at' => $this->brn_updated_at,
        ]);

        $query->andFilterWhere(['like', 'brn_title', $this->brn_title])
            ->andFilterWhere(['like', 'brn_descriptions', $this->brn_descriptions])
            ->andFilterWhere(['like', 'brn_image', $this->brn_image])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
