<?php

use common\models\Files;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .field-pages-logo, .field-pages-en_logo, .field-pages-chart, .field-pages-en_chart{
        display: inline-block;
    }
</style>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php Html::a(Yii::t('app', 'Create Pages'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::to(['pages/index'])]); ?>
    <?php
    if ($params){
    if ($params=='public-info'){?>
        <div class="col-md-12">
            <div class="col-md-6">
                <?= $form->field($model, 'logo')->fileInput() ;
                    $images=Files::findOne(['img_id'=>$model->logo]); ?>
                    <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/pages/<?= $images->img_image ?>" width="70" height="70">
<!--                    <img id="aImgShow" class="carImage" height="70px" width="70px">-->
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'en_logo')->fileInput() ;
                    $images=Files::findOne(['img_id'=>$model->en_logo]) ?>
                    <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/pages/<?= $images->img_image ?>" width="70" height="70">
<!--                    <img id="aImgShow" class="carImage" height="70px" width="70px">-->
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <?= $form->field($model, 'seo_title')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'labelTitle')->textInput() ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <?= $form->field($model, 'en_seo_title')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'en_labelTitle')->textInput() ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-sm-6">
                <?= $form->field($model, 'phone')->textInput() ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'email')->textInput() ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textInput() ?>
            <?= $form->field($model, 'en_description')->textInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'seo_description')->textInput() ?>
            <?= $form->field($model, 'en_seo_description')->textInput() ?>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <?= $form->field($model, 'telegram')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'instagram')->textInput() ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'address')->textInput() ?>
            <?= $form->field($model, 'en_address')->textInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'key_word')->textInput() ?>
            <?= $form->field($model, 'en_key_word')->textInput() ?>
        </div>
        <div class="col-md-12"></div>
    <?php
    }
    if ($params == 'strategic-program') {
        ?>
        <div class="col-md-12">
            <div class="col-md-6">
                <?= $form->field($model, 'chart')->fileInput() ;
                $images=Files::findOne(['img_id'=>$model->chart]); ?>
                <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/pages/<?= $images->img_image ?>" width="70" height="70">
                <!--                    <img id="aImgShow" class="carImage" height="70px" width="70px">-->
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'en_chart')->fileInput() ;
                $images=Files::findOne(['img_id'=>$model->en_chart]) ?>
                <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/pages/<?= $images->img_image ?>" width="70" height="70">
                <!--                    <img id="aImgShow" class="carImage" height="70px" width="70px">-->
            </div>
        </div>
        <?= $form->field($model, 'strategic_program')->widget(TinyMCE::className(), [
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?>
        <?= $form->field($model, 'en_strategic_program')->widget(TinyMCE::className(), [
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?>

        <?php
    }
    if ($params == 'about') {
        ?>
        <?= $form->field($model, 'about')->widget(TinyMCE::className(), [
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?>
        <?= $form->field($model, 'en_about')->widget(TinyMCE::className(), [
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?>
        <?php
    }
    if ($params == 'rule') {
        ?>
        <?= $form->field($model, 'rule')->widget(TinyMCE::className(), [
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?>
        <?= $form->field($model, 'en_rule')->widget(TinyMCE::className(), [
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?>

    <?php }
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php }else{
        echo "ثبت اطلاعات";
    } ?>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
<script>

    //view image whene uploaded
    var imageTypes = ['jpeg', 'jpg', 'png']; //Validate the images to show
    function showImage(src, target)
    {
        var fr = new FileReader();
        fr.onload = function(e)
        {
            target.src = this.result;
        };
        fr.readAsDataURL(src.files[0]);

    }

    var uploadImage = function(obj)
    {
        var val = obj.value;
        var lastInd = val.lastIndexOf('.');
        var ext = val.slice(lastInd + 1, val.length);
        if (imageTypes.indexOf(ext) !== -1)
        {
            var id = $(obj).data('target');
            var src = obj;
            var target = $(id)[0];
            showImage(src, target);
        }
        else
        {
            //اگر این قسمت خالی یاشد و این کدها نوشته نشه در اینصورت اگر عکس قبلا دانلود شده باشه دیگه اون زو نشون نمیده
            //متوتی تست کنی
            var id = $(obj).data('target');
            var src = obj;
            var target = $(id)[0];
            showImage(src, target);
        }
    }

</script>