<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use http\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
$url=Yii::$app->urlManager;
//var_dump($url=['site']);exit;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>miriam<?= Html::encode($this->title) ?></title>
    <script
        src="http://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <?php $this->head() ?>

    <?php
    //    $model = \app\models\Profiles::find()->where(['user_id'=>Yii::$app->user->getId()])->one();

    //    $const = new \backend\controllers\ManageConstant();
    ?>

</head>

<style>
    * {
        font-size: 12px;
    }

    .help-block {
        color: red;
    }

    th a {
        color: white;
    }

    .table-bordered:not(.detail-view) {
        text-align: center;
    }
</style>

<body class="dashboard fanum">

<?php $this->beginBody() ?>

<header class="mb-4 py-3 bg-white border-bottom fixed-top mb-5" style="z-index: 2!important;">
    <div class="container-fluid">
        <div class="row">
            <div class="d-none d-lg-block col-4 col-lg-3 col-xl-2"></div>
            <div class="col-12 col-8 col-lg-9 col-xl-10">
                <DIV CLASS="ROW">
                    <DIV CLASS="COL">

                    </DIV>

                </DIV>

            </div>
        </div>
    </div>
</header>
<br>
<div class="container-fluid" id="main-container mt-5" style="margin-top: 3.5rem">
    <div class="row" id="main-row">
        <div class="d-none d-lg-block col-4 col-lg-3 col-xl-2">
            <div class="sidebar bg-white shadow">
                <div class="sidebar-inner">
                    <div class="row h-100 no-gutters">
                        <div class="col">
                            <div class="sidebarlogo">
                                <a href=""><img src="" class="img-fluid"/></a>
                            </div>
                            <div class="text-center mt-4">
                                <div class="d-inline-block rounded-circle p-1 bg-white">
                                    <img src="<?= Yii::$app->homeUrl ?>dist/img/avatar.png" alt="avatar no photo"
                                         width="70"
                                         height="70"
                                         class="rounded-circle shadow img-fluid"/>
                                </div>

                            </div>
                            <h4 class="text-center mt-3"><a href="<?= Yii::$app->request->hostInfo ?>"
                                                            class="btn btn-warning btn-sm py-1 text-white">رفتن
                                    به صفحه اصلی سایت</a></h4>


                            <?php  if (!Yii::$app->user->isGuest){?>
                                <ul class="nav nav-mainmenu flex-column p-0 m-0 mt-3">
                                    <?php if(Yii::$app->user->can('admin_page')){ ?>
                                    <li class="nav-item ">
                                        <a class="nav-link rounded" data-toggle="collapse" href="#public">
                                            <i class="icofont icofont-lg align-middle">info</i><?= Yii::t('app', 'Public Information') ?></a>
                                        <div id="public" class="collapse in">
                                            <ul>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['pages/pages/','params'=>'public-info']) ?>">
                                                        <i class="icofont icofont-lg align-middle">info</i><?= Yii::t('app', 'Information') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['pages/pages/','params'=>'about']) ?>">
                                                        <i class="icofont icofont-lg align-middle">info</i><?= Yii::t('app', 'about') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['pages/pages/','params'=>'rule']) ?>">
                                                        <i class="icofont icofont-lg align-middle">info</i><?= Yii::t('app', 'rule') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['pages/pages/','params'=>'strategic-program']) ?>">
                                                        <i class="icofont icofont-lg align-middle">info</i><?= Yii::t('app', 'Strategic Plan') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['images/images/']) ?>">
                                                        <i class="icofont icofont-lg align-middle">image</i><?= Yii::t('app', 'Image') ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(Yii::$app->user->can('admin_user')){ ?>
                                    <li class="nav-item ">
                                        <a class="nav-link rounded" data-toggle="collapse" href="#demo">
                                            <i class="icofont icofont-lg align-middle">business_man_alt_1</i><?= Yii::t('app', 'Users') ?></a>
                                        <div id="demo" class="collapse in">

                                            <ul>

<!--                                                <li class="nav-item ">-->
<!--                                                    <a class="nav-link rounded" href="--><?php $url->createAbsoluteUrl(['user']) ?><!--">-->
<!--                                                        <i class="icofont icofont-lg align-middle">business_man_alt_1</i> --><?php Yii::t('app', 'Users') ?><!-- </a>-->
<!--                                                </li>-->

                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['user/create']) ?>">
                                                        <i class="icofont icofont-lg align-middle">business_man_alt_1</i><?= Yii::t('app', 'Create New User') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['/profiles/index']) ?>">
                                                        <i class="icofont icofont-lg align-middle">info_man_alt_1</i><?= Yii::t('app', 'Manege User') ?></a>
                                                </li>

                                            </ul>

                                        </div>
                                    </li>
                                    <?php  } ?>
                                    <?php if(Yii::$app->user->can('admin_service')){ ?>
                                    <li class="nav-item ">
                                        <a class="nav-link rounded" data-toggle="collapse" href="#services">
                                            <i class="icofont icofont-lg align-middle">shops</i>
                                            <?= Yii::t('app', 'Services') ?> </a>
                                        <div id="services" class="collapse in">
                                            <ul>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['services/create','lng'=>'Fa']) ?>">
                                                        <i class="icofont icofont-lg align-middle">alt_1</i><?= Yii::t('app', 'Create New Service In Persian Page') ?></a>
                                                </li>

                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['services/create','lng'=>'En']) ?>">
                                                        <i class="icofont icofont-lg align-middle">product_alt_1</i><?= Yii::t('app', 'Create New service In English Page') ?></a>
                                                </li>

                                            </ul>
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(Yii::$app->user->can('admin_blag')){ ?>
                                    <li class="nav-item ">
                                        <a class="nav-link rounded" data-toggle="collapse" href="#blag">
                                            <i class="icofont icofont-lg align-middle">book_man_alt_1</i><?= Yii::t('app', 'Blags') ?></a>
                                        <div id="blag" class="collapse in">
                                            <ul>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['blags/categories/create']) ?>">
                                                        <i class="icofont icofont-lg align-middle">_alt_1</i><?= Yii::t('app', 'Create New Category') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['blags/categories']) ?>">
                                                        <i class="icofont icofont-lg align-middle"></i><?= Yii::t('app', 'All Categories') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['blags/blags/create','lng'=>'Fa']) ?>">
                                                        <i class="icofont icofont-lg align-middle"></i><?= Yii::t('app', 'Create New Fa Blag') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['blags/blags/create','lng'=>'En']) ?>">
                                                        <i class="icofont icofont-lg align-middle"></i><?= Yii::t('app', 'Create New En Blag') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['blags/blags/']) ?>">
                                                        <i class="icofont icofont-lg align-middle"></i><?= Yii::t('app', 'All Blags') ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(Yii::$app->user->can('admin_slider')){ ?>
                                        <li class="nav-item ">
                                        <a class="nav-link rounded" data-toggle="collapse" href="#slider">
                                            <i class="icofont icofont-lg align-middle">image_alt_1</i>
                                            <?= Yii::t('app', 'Sliders') ?></a>
                                        <div id="slider" class="collapse in">
                                            <ul>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['/sliders/index']) ?>">
                                                        <i class="icofont icofont-lg align-middle">image_alt_1</i><?= Yii::t('app', 'All Slider') ?></a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['/links/index']) ?>">
                                                        <i class="icofont icofont-lg align-middle">image_alt_1</i><?= Yii::t('app', 'All Links') ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(Yii::$app->user->can('admin_comment')){ ?>
                                    <li class="nav-item ">
                                        <a class="nav-link rounded" data-toggle="collapse" href="#com">
                                            <i class="icofont icofont-lg align-middle">all_comment_alt_1</i>
                                            <?= Yii::t('app', 'Users Comments') ?></a>
                                        <div id="com" class="collapse in">

                                            <ul>

                                                <li class="nav-item ">
                                                    <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['/comments/index']) ?>">
                                                        <i class="icofont icofont-lg align-middle">business_man_alt_1</i> <?= Yii::t('app', 'All Comments') ?></a>
                                                </li>
                                            </ul>

                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php if(Yii::$app->user->can('admin_page')){ ?>

                                    <?php } ?>
                                    <div class="sidebar-footer text-center">
                                        <div class="d-inline-block bg-light text-muted small rounded px-2"><span
                                                class="fanum"></span></div>
                                    </div>


                                    <?php echo Html::beginForm(['site/logout']); ?>
                                    <?= Html::submitButton('<span>'. Yii::t('app', 'Exit').'</span>',['class'=>'accordion ex-bt']) ?>
                                    <?php echo Html::endForm() ?>

                                </ul>


                            <?php }else{ ?>
                                <li class="nav-item ">
                                    <a class="nav-link rounded" data-toggle="collapse" href="#demo">
                                        <i class="icofont icofont-lg align-middle">business_man_alt_1</i><?= Yii::t('app', 'Users') ?></a>
                                    <div id="demo" class="collapse in">

                                        <ul>

                                            <li class="nav-item ">
                                                <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['user']) ?>">
                                                    <i class="icofont icofont-lg align-middle">business_man_alt_1</i> <?= Yii::t('app', 'Users') ?> </a>
                                            </li>

                                            <li class="nav-item ">
                                                <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['site/signup']) ?>">
                                                    <i class="icofont icofont-lg align-middle">business_man_alt_1</i><?= Yii::t('app', 'Create New User') ?></a>
                                            </li>
                                            <li class="nav-item ">
                                                <a class="nav-link rounded" href="<?= $url->createAbsoluteUrl(['/profiles/index']) ?>">
                                                    <i class="icofont icofont-lg align-middle">info_man_alt_1</i><?= Yii::t('app', 'Manege User') ?></a>
                                            </li>

                                        </ul>

                                    </div>
                                </li>

                                <?php echo Html::beginForm(['site/logout']); ?>
                                <?= Html::submitButton('<span>'. Yii::t('app', 'Exit').'</span>',['class'=>'accordion ex-bt']) ?>
                                <?php echo Html::endForm() ?>
                            <?php  }  ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-8 col-lg-9 col-xl-10" id="page-content-area">
            <div id="page-content" class="px-sm-3">
                <div class="card p-3 mr-5">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="bg-light copyright mr-3">
            <div class="row align-items-center mr-5">
                <div class="d-none d-lg-block col-4 col-lg-3 col-xl-2"></div>
                <div class="col">
                    <div class="text-muted">تمامی حقوق پلتفرم <b>miriam</b> برای شرکت <b class="text-info">پسته </b> میباشد محفوظ
                        است. هر گونه کپی برداری یا استفاده از متد های پلتفرم <b>miriam</b> با پیگرد قانونی همراه خواهد
                        بود.
                    </div>
                </div>
            </div>
        </div>
        <?php
        $pagination = <<< JS
// var dropdown=document.getElementsByClassName('dropdown');
// var i;
// for (i=0;i<dropdown.length;i++){
//     dropdown[i].addEventListener('click',function() {
//         this.classList.toggle('active');
//         var dropdownContent=this.nextElementSibling;
//         if(dropdownContent.style.display==='block'){
//             dropdownContent.style.display='none';
//         }else {
//             dropdownContent.style.display='block'
//         }
//      
//     });
// }

// $('ul.pagination > li').addClass('page-item');
// $('ul.pagination > li > a').addClass('page-link');
// $('ul.pagination > li > span').addClass('page-link')
// $('ul.pagination').css('direction','ltr')
// $('ul.pagination').addClass('float-right')
JS;

        $this->registerJs($pagination, \yii\web\View::POS_END);
        ?>
        <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
    var openInbox = document.getElementById("myBtn");
    openInbox.click();

    function khoy_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";

    }

    function khoy_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";

    }

    function myFunc(id) {
        var x = document.getElementById(id);
        if (x.className.indexOf("khoy-show") == -1) {
            x.className += " khoy-show";
            x.previousElementSibling.className += " khoy-red";
        } else {
            x.className = x.className.replace(" khoy-show", "");
            x.previousElementSibling.className =
                x.previousElementSibling.className.replace(" khoy-red", "");
        }
    }

    openMail("Borge")
    function openMail(personName) {
        var i;
        var x = document.getElementsByClassName("person");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x = document.getElementsByClassName("test");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" khoy-light-grey", "");
        }
        document.getElementById(personName).style.display = "block";
        event.currentTarget.className += " khoy-light-grey";
    }
</script>
