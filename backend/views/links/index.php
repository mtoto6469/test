<?php

use common\models\Links;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LinksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Links');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $model1=new Links()?>
    <?php $form = ActiveForm::begin(['action'=>'create']); ?>
<div class="col-md-12">
    <div class="col-md-6"><?= $form->field($model1, 'lnk_title')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-6"><?= $form->field($model1, 'lnk_link')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-6"><?= $form->field($model1, 'lnk_lng')->radioList([0=>Yii::t('app','Fa'),1=>Yii::t('app','En')]) ?>
    </div>
</div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'lnkUser.username',
            'lnk_title',
            'lnk_link',
            'lnk_lng',


            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
