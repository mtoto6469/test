<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Links */

$this->title = Yii::t('app', 'Update Links: {name}', [
    'name' => $model->lnk_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lnk_id, 'url' => ['view', 'id' => $model->lnk_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="links-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model1' => $model,
    ]) ?>

</div>
