<?php

use common\models\Files;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faravaghi\jalaliDatePicker\jalaliDatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Profiles */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .field-profiles-image{
        display: inline-block;
    }
</style>
<div class="profiles-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-md-12">
        <div class="col-md-6"><?= $form->field($model, 'pro_fullName')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'pro_en_fullName')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><?= $form->field($model, 'image')->fileInput(['onchange'=>"uploadImage(this)",'data-target'=>'#aImgShow']) ?>
            <?php if (!$model->isNewRecord){
            $images=Files::findOne(['img_id'=>$model->pro_image_id]) ?>
                 <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/users/<?= $images->img_image ?>" width="70" height="70">
           <?php }else{?>
                <img id="aImgShow" class="carImage" height="70px" width="70px">
            <?php }  ?>
        </div>
        <div class="col-md-6"><?= $form->field($model, 'pro_mobile')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"> <?= $form->field($model, 'pro_position')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"> <?= $form->field($model, 'pro_en_position')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><?= $form->field($model, 'pro_degree')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'pro_en_degree')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><?php $form->field($model, 'pro_province')->dropDownList(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?php $form->field($model, 'pro_city')->dropDownList(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12"><?php $form->field($model, 'pro_address')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12"><?php $form->field($model, 'pro_en_address')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12">
        <div class="col-md-6"> <?= $form->field($model, 'pro_birthday')->widget(jalaliDatePicker::className(),[
                'options' => array(
                        'format' => 'yyyy/mm/dd',
                        'viewformat' => 'yyyy/mm/dd',
                        'placement' => 'left',
                        'todayBtn'=> 'linked',
                      ),
                       'htmlOptions' => [
                            'id' => 'date',
                            'class' => 'form-control add-in1',
                       ]
                    ])->label(Yii::t('app','birthday'))?></div>
        <div class="col-md-6"><?= $form->field($model, 'pro_phone')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12"><?= $form->field($model, 'pro_gender')->radioList(['1'=>Yii::t('app','Female'),'2'=>Yii::t('app','Male')]) ?></div>
    <div class="col-md-12">
        <?php $form->field($model, 'pro_biography')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'pro_biography')->widget(TinyMCE::className(),[
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><?= $form->field($model, 'pro_descriptions')->textarea(['rows' => 6]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'pro_en_descriptions')->textarea(['rows' => 6]) ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"> <?= $form->field($model, 'pro_nationalCode')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'pro_postalCode')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12"><?= $form->field($model, 'pro_status')->checkbox() ?></div>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    var imageTypes = ['jpeg', 'jpg', 'png']; //Validate the images to show
    function showImage(src, target)
    {
        var fr = new FileReader();
        fr.onload = function(e)
        {
            target.src = this.result;
        };
        fr.readAsDataURL(src.files[0]);

    }

    var uploadImage = function(obj)
    {
        var val = obj.value;
        var lastInd = val.lastIndexOf('.');
        var ext = val.slice(lastInd + 1, val.length);
        if (imageTypes.indexOf(ext) !== -1)
        {
            var id = $(obj).data('target');
            var src = obj;
            var target = $(id)[0];
            showImage(src, target);
        }
        else
        {
            //اگر این قسمت خالی یاشد و این کدها نوشته نشه در اینصورت اگر عکس قبلا دانلود شده باشه دیگه اون زو نشون نمیده
            //متوتی تست کنی
            var id = $(obj).data('target');
            var src = obj;
            var target = $(id)[0];
            showImage(src, target);
        }
    }
</script>