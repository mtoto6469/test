<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Profiles */

$this->title = $model->pro_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>img {
        width: 50px;
        height: 50px
    }</style>
<div class="profiles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pro_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pro_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'proUser.username',
            'pro_fullName',
            [
                'attribute' => 'pro_image_id',
                'value' => function ($model) {
                    return Yii::$app->main->showImage($model->pro_image_id, 'users');
                },
                'format' => 'image',
            ],
            'pro_position',
            'pro_degree',
            'pro_birthday',
            [
                'attribute' => 'pro_gender',
                'value' => function ($model) {
                    return Yii::$app->main->gender($model->pro_gender);
                },
                'label' => Yii::t('app', 'Pro Gender')
            ],
            'pro_phone',
            'pro_mobile',
            'pro_nationalCode',
            'pro_postalCode',
//            'pro_province',
//            'pro_city',
            'pro_address',
            'pro_descriptions',
            [
                'attribute' => 'pro_biography',
                'format' => 'raw'
            ],
//            'pro_valet',
//            'pro_status',
            'pro_created_at',
            'pro_updated_at',
        ],
    ]) ?>

</div>
