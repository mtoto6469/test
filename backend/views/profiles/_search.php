<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProfilesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profiles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pro_id') ?>

    <?= $form->field($model, 'pro_user_id') ?>

    <?= $form->field($model, 'pro_fullName') ?>

    <?= $form->field($model, 'pro_image_id') ?>

    <?= $form->field($model, 'pro_position') ?>

    <?php // echo $form->field($model, 'pro_degree') ?>

    <?php // echo $form->field($model, 'pro_birthday') ?>

    <?php // echo $form->field($model, 'pro_gender') ?>

    <?php // echo $form->field($model, 'pro_phone') ?>

    <?php // echo $form->field($model, 'pro_mobile') ?>

    <?php // echo $form->field($model, 'pro_nationalCode') ?>

    <?php // echo $form->field($model, 'pro_postalCode') ?>

    <?php // echo $form->field($model, 'pro_province') ?>

    <?php // echo $form->field($model, 'pro_city') ?>

    <?php // echo $form->field($model, 'pro_address') ?>

    <?php // echo $form->field($model, 'pro_descriptions') ?>

    <?php // echo $form->field($model, 'pro_biography') ?>

    <?php // echo $form->field($model, 'pro_valet') ?>

    <?php // echo $form->field($model, 'pro_status') ?>

    <?php // echo $form->field($model, 'pro_created_at') ?>

    <?php // echo $form->field($model, 'pro_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
