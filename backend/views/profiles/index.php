<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProfilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Profiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>img {
        width: 50px;
        height: 50px
    }</style>
<div class="profiles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Profiles'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'pro_id',
            'proUser.username',
            'pro_fullName',
            'pro_position',
            //'pro_degree',
            //'pro_birthday',
            //'pro_gender',
            //'pro_phone',
            'pro_mobile',
            [
                'attribute' => 'pro_image_id',
                'value' => function ($model) {
                    return Yii::$app->main->showImage($model->pro_image_id, 'users');
                },
                'format' => 'image',
            ],
            //'pro_nationalCode',
            //'pro_postalCode',
            //'pro_province',
            //'pro_city',
            //'pro_address',
            //'pro_descriptions',
            //'pro_biography:ntext',
            //'pro_valet',
            //'pro_status',
            //'pro_created_at',
            //'pro_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
