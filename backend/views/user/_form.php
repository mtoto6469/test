<?php

use backend\models\Role;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <label class="control-label" for="user-status">role</label>
    <?= Html::activeDropDownList($model,'auth_item',ArrayHelper::map(Role::find()->where(['type'=>1])->andWhere(['<>','name','developer_admin'])->all(),'name','name'),['class'=>'form-control']) ?>
    <?php Html::activeDropDownList($model,'auth_item',ArrayHelper::map(Role::findAll(['type'=>1]),'name','name'),['class'=>'form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
