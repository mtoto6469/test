<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#user">user</a></li>
        <li><a data-toggle="tab" href="#role">role</a></li>

    </ul>

    <div class="tab-content">
        <div id="user" class="tab-pane fade in active">
            <div class="user-index">

                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>

                <?php Pjax::begin(); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProviderUser,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

//                        'id',
                        'username',
//                        'auth_key',
//                        'password_hash',
//                        'password_reset_token',
                        //'email:email',
                        'status',
                        //'created_at',
                        //'updated_at',
                        //'verification_token',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>

                <?php Pjax::end(); ?>
            </div>
        </div>
        <div id="role" class="tab-pane fade in active">
            <div class="user-index">

                <?php $this->title='Roles'; ?>
                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a(Yii::t('app', 'Create Role'), ['role/create'], ['class' => 'btn btn-success']) ?>
                </p>

                <?php Pjax::begin(); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProviderRole,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

//                        'id',
                        'name',
//                        'type',
                        'description',

                        [
                                'class' => 'yii\grid\ActionColumn',
                                 'controller'=>'role'
                        ],
                    ],
                ]); ?>

                <?php Pjax::end(); ?>
            </div>
        </div>

    </div>

</div>
