<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Likes */

$this->title = Yii::t('app', 'Update Likes: {name}', [
    'name' => $model->lik_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Likes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lik_id, 'url' => ['view', 'id' => $model->lik_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="likes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
