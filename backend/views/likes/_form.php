<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Likes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="likes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lik_user_id')->textInput() ?>

    <?= $form->field($model, 'lik_computer_ip')->textInput() ?>

    <?= $form->field($model, 'lik_blag_id')->textInput() ?>

    <?= $form->field($model, 'lik_comment_id')->textInput() ?>

    <?= $form->field($model, 'lik_like')->textInput() ?>

    <?= $form->field($model, 'lik_created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
