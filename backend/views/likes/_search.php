<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LikesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="likes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'lik_id') ?>

    <?= $form->field($model, 'lik_user_id') ?>

    <?= $form->field($model, 'lik_computer_ip') ?>

    <?= $form->field($model, 'lik_blag_id') ?>

    <?= $form->field($model, 'lik_comment_id') ?>

    <?php // echo $form->field($model, 'lik_like') ?>

    <?php // echo $form->field($model, 'lik_created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
