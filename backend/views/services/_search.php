<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ServicesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="services-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'srv_id') ?>

    <?= $form->field($model, 'srv_user_id') ?>

    <?= $form->field($model, 'srv_category') ?>

    <?= $form->field($model, 'srv_title') ?>

    <?= $form->field($model, 'srv_body') ?>

    <?php // echo $form->field($model, 'srv_image_id') ?>

    <?php // echo $form->field($model, 'srv_location') ?>

    <?php // echo $form->field($model, 'srv_project_time_start') ?>

    <?php // echo $form->field($model, 'srv_project_time_end') ?>

    <?php // echo $form->field($model, 'srv_boss') ?>

    <?php // echo $form->field($model, 'srv_keyword') ?>

    <?php // echo $form->field($model, 'srv_meta_descriptions') ?>

    <?php // echo $form->field($model, 'srv_meta_title') ?>

    <?php // echo $form->field($model, 'pro_link') ?>

    <?php // echo $form->field($model, 'srv_view_count') ?>

    <?php // echo $form->field($model, 'srv_like_count') ?>

    <?php // echo $form->field($model, 'srv_comment_count') ?>

    <?php // echo $form->field($model, 'srv_status') ?>

    <?php // echo $form->field($model, 'srv_created_at') ?>

    <?php // echo $form->field($model, 'srv_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
