<?php

use common\models\Files;
use dosamigos\tinymce\TinyMce;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Services */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="services-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="col-md-12">
        <div class="col-md-6"><?= $form->field($model, 'image')->fileInput(['onchange'=>"uploadImage(this)",'data-target'=>'#aImgShow']) ?>
            <?php if (!$model->isNewRecord){
                $images=Files::findOne(['img_id'=>$model->srv_image_id]) ?>
                <img id="aImgShow" class="carImage" src="<?= Yii::$app->request->hostInfo ?>/upload/pages/<?= $images->img_image ?>" width="70" height="70">
            <?php }else{?>
                <img id="aImgShow" class="carImage" height="70px" width="70px">
            <?php }  ?>
        </div>
        <div class="col-md-6"> <?= $form->field($model, 'srv_category')->dropDownList(['Progress Project' => Yii::t('app','Progress Project'),'Completion Project'=>Yii::t('app','Completion Project')]) ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><?= $form->field($model, 'srv_title')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'srv_boss')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"> <?= $form->field($model, 'srv_project_time_end')->widget(jalaliDatePicker::className(),[
                'options' => array(
                        'format' => 'yyyy/mm/dd',
                        'viewformat' => 'yyyy/mm/dd',
                        'placement' => 'left',
                        'todayBtn'=> 'linked',
                      ),
                       'htmlOptions' => [
                            'id' => 'date',
                            'class' => 'form-control add-in1',
                       ]
                    ])?></div>
        <div class="col-md-6"> <?= $form->field($model, 'srv_project_time_start')->widget(jalaliDatePicker::className(),[
                'options' => array(
                    'format' => 'yyyy/mm/dd',
                    'viewformat' => 'yyyy/mm/dd',
                    'placement' => 'left',
                    'todayBtn'=> 'linked',
                ),
                'htmlOptions' => [
                    'id' => 'date1',
                    'class' => 'form-control add-in1',
                ]
            ])?></div>
    <div class="col-md-12"><?= $form->field($model, 'srv_location')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12"> <?= $form->field($model, 'srv_body')->widget(TinyMCE::className(),[
            'options' => ['rows' => 30],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/admin/plugins/responsivefilemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/admin/plugins/responsivefilemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]) ?></div>
    <div class="col-md-12">
        <div class="col-md-6"> <?= $form->field($model, 'srv_meta_title')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'srv_link')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="col-md-12"> <?= $form->field($model, 'srv_meta_descriptions')->textInput(['maxlength' => true]) ?></div>
    <div class="col-md-12"> <?= $form->field($model, 'srv_keyword')->textInput(['maxlength' => true]) ?></div>
<!--    <div class="col-md-12"> --><?//= $form->field($model, 'srv_status')-> ?><!--</div>-->
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>



    <script>
        var imageTypes = ['jpeg', 'jpg', 'png']; //Validate the images to show
        function showImage(src, target)
        {
            var fr = new FileReader();
            fr.onload = function(e)
            {
                target.src = this.result;
            };
            fr.readAsDataURL(src.files[0]);

        }

        var uploadImage = function(obj)
        {
            var val = obj.value;
            var lastInd = val.lastIndexOf('.');
            var ext = val.slice(lastInd + 1, val.length);
            if (imageTypes.indexOf(ext) !== -1)
            {
                var id = $(obj).data('target');
                var src = obj;
                var target = $(id)[0];
                showImage(src, target);
            }
            else
            {
                //اگر این قسمت خالی یاشد و این کدها نوشته نشه در اینصورت اگر عکس قبلا دانلود شده باشه دیگه اون زو نشون نمیده
                //متوتی تست کنی
                var id = $(obj).data('target');
                var src = obj;
                var target = $(id)[0];
                showImage(src, target);
            }
        }
    </script>