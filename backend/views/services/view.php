<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Services */

$this->title = $model->srv_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="services-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->srv_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->srv_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'srv_id',
            'srv_user_id',
            'srv_category',
            'srv_title',
            'srv_body:ntext',
            'srv_image_id',
            'srv_location',
//            'srv_project_time_start:datetime',
//            'srv_project_time_end:datetime',
            'srv_project_time_start',
            'srv_project_time_end',
            'srv_boss',
            'srv_keyword',
            'srv_meta_descriptions',
            'srv_meta_title',
            'srv_link',
            'srv_view_count',
            'srv_like_count',
            'srv_comment_count',
            'srv_status',
            'srv_created_at',
            'srv_updated_at',
        ],
    ]) ?>

</div>
