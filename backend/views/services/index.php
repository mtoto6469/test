<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Services');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Services'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'srv_id',
            'srv_user_id',
            'srv_category',
            'srv_title',
            'srv_body:ntext',
            //'srv_image_id',
            //'srv_location',
            //'srv_project_time_start:datetime',
            //'srv_project_time_end:datetime',
            //'srv_boss',
            //'srv_keyword',
            //'srv_meta_descriptions',
            //'srv_meta_title',
            //'pro_link',
            //'srv_view_count',
            //'srv_like_count',
            //'srv_comment_count',
            //'srv_status',
            //'srv_created_at',
            //'srv_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
