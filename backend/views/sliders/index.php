<?php

use common\models\Files;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProfilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sliders');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>img {
        width: 50px;
        height: 50px
    }</style>
<div class="profiles-index">

    <h1><?= Html::encode($this->title) ?></h1>
<?php $model1=new Files();  ?>
    <?php $form = ActiveForm::begin(['action'=>'create']); ?>

    <?= $form->field($model1, 'img_image')->fileInput()?>

    <img id="aImgShow" class="carImage"  width="70" height="70">


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'img_id',
            [
                'attribute' => 'img_id',
                'value' => function ($model) {
                    return Yii::$app->main->showImage($model->img_id, 'sliders');
                },
                'format' => 'image',
            ],

            //'pro_status',


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',

                ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
