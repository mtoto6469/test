<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'language'=>'fa-IR',
    'bootstrap' => ['log'],
//    'modules' => [],
//    'i18n' => [
//        'translations' => [
//            'admin*' => [
//                'class' => 'yii\i18n\PhpMessageSource',
////                    'sourceLanguage' => 'fa-FA',
//                'basePath' => '@common/messages'
//            ],
//        ],
//    ],
    'modules' => [
        'blags' => [
            'class' => 'backend\modules\blags\Blags',
        ],
        'products' => [
            'class' => 'backend\modules\products\Products',
        ],
    ],
    'components' => [


        'request' => [
            'baseUrl' => '/admin',
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
//            'class'=>'common\component\ZurlManager',//for multilingual
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'pages/pages'=>'pages/index',
//                '<language:\w+>/'=>'site/index',//for multilingual
//                '<language:\w+>/<action>'=>'site/<action>',//for multilingual
//                '<language:\w+>/<controller>/<action>'=>'<controller>/<action>',//for multilingual
            ],
        ],

    ],
    'params' => $params,
];
