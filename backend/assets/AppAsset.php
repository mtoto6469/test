<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/dist/css/bootstrap.min.css',
        'css/dist/css/icofont.min.css',
        'css/dist/css/jquery.mCustomScrollbar.min.css',
        'css/dist/css/base.css',
        'css/dist/css/rtl.css',
        'css/dist/css/dashboard.css',
        'css/dist/css/jqx.base.css',
        'css/dist/css/jqx.darkblue.css',
        'css/dist/css/slick.css',
        'css/dist/css/slick-theme.css',
        'css/dist/css/persian-datepicker.min.css',
        'css/dist/js/jquery-3.2.1.min.js',
        'css/dist/css/order.css',
        'css/dist/css/bootstrap-treeview.css',
        'css/dist/css/all.min.css',
        'css/dist/css/select2.min.css',
    ];
    public $js = [
//        'css/dist/js/bootstrap.min.js',
        'js/miriam.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
