<?php

namespace backend\models;

use common\components\AuthorRule;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "tbl_auth_item".
 *
 * @property string $name
 * @property int $type
 * @property string|null $description
 * @property string|null $rule_name
 * @property resource|null $data
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 * @property Role[] $children
 * @property Role[] $parents
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_auth_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthorRule::className(), 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'rule_name' => Yii::t('app', 'Rule Name'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AuthAssignments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * Gets query for [[RuleName]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * Gets query for [[AuthItemChildren]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * Gets query for [[AuthItemChildren0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }

    /**
     * Gets query for [[Children]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Role::className(), ['name' => 'child'])->viaTable('tbl_auth_item_child', ['parent' => 'name']);
    }

    /**
     * Gets query for [[Parents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(Role::className(), ['name' => 'parent'])->viaTable('tbl_auth_item_child', ['child' => 'name']);
    }
    private function all_roles(){

        return[
          'categories'=>[
              ['name'=>'admin_category','label'=>'admin','checked'=>0],
              ['name'=>'add_category','label'=>'create','checked'=>0],
              ['name'=>'update_category','label'=>'update','checked'=>0],
              ['name'=>'delete_category','label'=>'delete','checked'=>0],
             // ['name'=>'view_category','label'=>'view_category','checked'=>0],
          ],
          'blags'=>[
              ['name'=>'admin_blag','label'=>'admin','checked'=>0],
              ['name'=>'add_blag','label'=>'create','checked'=>0],
              ['name'=>'update_blag','label'=>'update','checked'=>0],
              ['name'=>'delete_blag','label'=>'delete','checked'=>0],
             // ['name'=>'view_blag','label'=>'view_blag','checked'=>0],
          ],
          'sliders'=>[
              ['name'=>'admin_slider','label'=>'admin','checked'=>0],
              ['name'=>'add_slider','label'=>'create','checked'=>0],
              ['name'=>'update_slider','label'=>'update','checked'=>0],
              ['name'=>'delete_slider','label'=>'delete','checked'=>0],
             // ['name'=>'view_slider','label'=>'view_slider','checked'=>0],
          ],
          'comments'=>[
              ['name'=>'admin_comment','label'=>'admin','checked'=>0],
              ['name'=>'add_comment','label'=>'create','checked'=>0],
              ['name'=>'update_comment','label'=>'update','checked'=>0],
              ['name'=>'delete_comment','label'=>'delete','checked'=>0],
            //  ['name'=>'view_comment','label'=>'view_comment','checked'=>0],
          ],
          'likes'=>[
              ['name'=>'admin_like','label'=>'admin','checked'=>0],
              ['name'=>'add_like','label'=>'create','checked'=>0],
              ['name'=>'update_like','label'=>'update','checked'=>0],
              ['name'=>'delete_like','label'=>'delete','checked'=>0],
              //['name'=>'view_like','label'=>'view_like','checked'=>0],
          ],
          'pages'=>[
              ['name'=>'admin_page','label'=>'admin','checked'=>0],
              ['name'=>'add_page','label'=>'create','checked'=>0],
              ['name'=>'update_page','label'=>'update','checked'=>0],
              ['name'=>'delete_page','label'=>'delete','checked'=>0],
             // ['name'=>'view_page','label'=>'view_page','checked'=>0],
          ],
          'files'=>[
              ['name'=>'admin_file','label'=>'admin','checked'=>0],
              ['name'=>'add_file','label'=>'create','checked'=>0],
              ['name'=>'update_file','label'=>'update','checked'=>0],
              ['name'=>'delete_file','label'=>'delete','checked'=>0],
            //  ['name'=>'view_file','label'=>'view_image','checked'=>0],
          ],
          'pro_categories'=>[
              ['name'=>'admin_pro_category','label'=>'admin','checked'=>0],
              ['name'=>'add_pro_category','label'=>'create','checked'=>0],
              ['name'=>'update_pro_category','label'=>'update','checked'=>0],
              ['name'=>'delete_pro_category','label'=>'delete','checked'=>0],
             // ['name'=>'view_pro_category','label'=>'view_pro_category','checked'=>0],
          ],
          'colors'=>[
              ['name'=>'admin_color','label'=>'admin','checked'=>0],
              ['name'=>'add_color','label'=>'create','checked'=>0],
              ['name'=>'update_color','label'=>'update','checked'=>0],
              ['name'=>'delete_color','label'=>'delete','checked'=>0],
            //  ['name'=>'view_color','label'=>'view_color','checked'=>0],
          ],
          'brands'=>[
              ['name'=>'admin_brand','label'=>'admin','checked'=>0],
              ['name'=>'add_brand','label'=>'create','checked'=>0],
              ['name'=>'update_brand','label'=>'update','checked'=>0],
              ['name'=>'delete_brand','label'=>'delete','checked'=>0],
             // ['name'=>'view_brand','label'=>'view_brand','checked'=>0],
          ],
          'sizes'=>[
              ['name'=>'admin_size','label'=>'admin','checked'=>0],
              ['name'=>'add_size','label'=>'create','checked'=>0],
              ['name'=>'update_size','label'=>'update','checked'=>0],
              ['name'=>'delete_size','label'=>'delete','checked'=>0],
            //  ['name'=>'view_size','label'=>'view_size','checked'=>0],
          ],
          'suppliers'=>[
              ['name'=>'admin_supplier','label'=>'admin','checked'=>0],
              ['name'=>'add_supplier','label'=>'create','checked'=>0],
              ['name'=>'update_supplier','label'=>'update','checked'=>0],
              ['name'=>'delete_supplier','label'=>'delete','checked'=>0],
           //  ['name'=>'view_supplier','label'=>'view_supplier','checked'=>0],
          ],
          'products'=>[
              ['name'=>'admin_product','label'=>'admin','checked'=>0],
              ['name'=>'add_product','label'=>'create','checked'=>0],
              ['name'=>'update_product','label'=>'update','checked'=>0],
              ['name'=>'delete_product','label'=>'delete','checked'=>0],
             // ['name'=>'view_product','label'=>'view_product','checked'=>0],
          ],
            'wish_lists'=>[
                ['name'=>'admin_wish_list','label'=>'admin','checked'=>0],
                ['name'=>'add_wish_list','label'=>'create','checked'=>0],
                ['name'=>'update_wish_list','label'=>'update','checked'=>0],
                ['name'=>'delete_wish_list','label'=>'delete','checked'=>0],
                // ['name'=>'view_product','label'=>'view_product','checked'=>0],
            ],
            'orders'=>[
                ['name'=>'admin_orders','label'=>'admin','checked'=>0],
                ['name'=>'add_orders','label'=>'create','checked'=>0],
                ['name'=>'update_orders','label'=>'update','checked'=>0],
                ['name'=>'delete_orders','label'=>'delete','checked'=>0],
                // ['name'=>'view_product','label'=>'view_product','checked'=>0],
            ],
            'order_details'=>[
                ['name'=>'admin_order_detail','label'=>'admin','checked'=>0],
                ['name'=>'add_order_detail','label'=>'create','checked'=>0],
                ['name'=>'update_order_detail','label'=>'update','checked'=>0],
                ['name'=>'delete_order_detail','label'=>'delete','checked'=>0],
                // ['name'=>'view_product','label'=>'view_product','checked'=>0],
            ],
            'order_payments'=>[
                ['name'=>'admin_order_payment','label'=>'admin','checked'=>0],
                ['name'=>'add_order_payment','label'=>'create','checked'=>0],
                ['name'=>'update_order_payment','label'=>'update','checked'=>0],
                ['name'=>'delete_order_payment','label'=>'delete','checked'=>0],
                // ['name'=>'view_product','label'=>'view_product','checked'=>0],
            ],
            'valets'=>[
                ['name'=>'admin_valet','label'=>'admin','checked'=>0],
                ['name'=>'add_valet','label'=>'create','checked'=>0],
                ['name'=>'update_valet','label'=>'update','checked'=>0],
                ['name'=>'delete_valet','label'=>'delete','checked'=>0],
                // ['name'=>'view_product','label'=>'view_product','checked'=>0],
            ],

          'user'=>[
                ['name'=>'admin_user','label'=>'admin','checked'=>0],
                ['name'=>'add_user','label'=>'create','checked'=>0],
                ['name'=>'update_user','label'=>'update','checked'=>0],
                ['name'=>'delete_user','label'=>'delete','checked'=>0],
                // ['name'=>'view_user','label'=>'view_user','checked'=>0],
            ],
            'services'=>[
                ['name'=>'admin_service','label'=>'admin','checked'=>0],
                ['name'=>'add_service','label'=>'create','checked'=>0],
                ['name'=>'update_service','label'=>'update','checked'=>0],
                ['name'=>'delete_service','label'=>'delete','checked'=>0],
                // ['name'=>'view_service','label'=>'view_user','checked'=>0],
            ],
            'themes'=>[
                ['name'=>'admin_theme','label'=>'admin','checked'=>0],
                ['name'=>'add_theme','label'=>'create','checked'=>0],
                ['name'=>'update_theme','label'=>'update','checked'=>0],
                ['name'=>'delete_theme','label'=>'delete','checked'=>0],
                // ['name'=>'view_theme','label'=>'view_user','checked'=>0],
            ],
        ];
    }
    //فرستادن دیتا برای ساخت checkBoxList که توسط اون کاربر بیاد و یکی یکی انتخاب کنه
    public function getAllRoles()
    {
        $roles=$this->all_roles();
        if (!$this->isNewRecord){
            $db_all_rules=(new Query())
                ->select(['child'])
                ->from('tbl_auth_item_child')
                ->where(['parent'=>$this->name])
                ->all();
            $db_roles=[];
            foreach ($db_all_rules as $key=>$value)
            {
                array_push($db_roles,$value['child']);
            }
            foreach ($roles as $key=>$value)
            {
                foreach ($value as $keyItem=>$valueItem)
                {
                    if(in_array($valueItem['name'],$db_roles))
                    {
                        $roles[$key][$keyItem]['checked']=1;
                    }
                }

            }
        }
        return $roles;

    }
    public function save($runValidation = true, $attributeNames = NULL)
    {
        $auth=Yii::$app->authManager;
        $time=time();
        $sql="DELETE FROM `tbl_auth_item_child` WHERE `parent`='{$this->name}'";
        Yii::$app->db->createCommand($sql)->query();

        $items=Yii::$app->request->post('Items');

        $sql="INSERT IGNORE INTO `tbl_auth_item` (`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) VALUES ('{$this->name}',1,'{$this->description}',null ,null , $time, $time)";
        Yii::$app->db->createCommand($sql)->query();

        if ($items != null){
        foreach ($items as $key=>$value ){

            $sql="INSERT IGNORE INTO `tbl_auth_item` (`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) VALUES ('{$key}',2,'{$key}',null ,null , $time, $time)";
            Yii::$app->db->createCommand($sql)->query();

            $sql="INSERT INTO `tbl_auth_item_child`(`parent`, `child`) VALUES ('{$this->name}','{$key}')";
            Yii::$app->db->createCommand($sql)->query();


        }
        }

        return true;

    }
}
