<?php

namespace backend\models;

use common\models\SignupForm;
use Yii;

/**
 * This is the model class for table "tbl_user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $verification_token
 *
 * @property Blags[] $blags
 * @property Brands[] $brands
 * @property Categories[] $categories
 * @property Colors[] $colors
 * @property Comments[] $comments
 * @property Likes[] $likes
 * @property ProCategories[] $proCategories
 * @property Products[] $products
 * @property Profiles[] $profiles
 */
class User extends \yii\db\ActiveRecord
{
    public  $auth_item;
    public $password;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_user';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at','auth_item','password'], 'required','on'=>'newUser'],
            [['username', 'email','auth_item'], 'required','on'=>'updateUser'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'verification_token','password'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password' => Yii::t('app', 'Password'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'verification_token' => Yii::t('app', 'Verification Token'),
        ];
    }

    /**
     * Gets query for [[Blags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBlags()
    {
        return $this->hasMany(Blags::className(), ['blg_user_id' => 'id']);
    }

    /**
     * Gets query for [[Brands]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrands()
    {
        return $this->hasMany(Brands::className(), ['brn_user_id' => 'id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['cat_user_id' => 'id']);
    }

    /**
     * Gets query for [[Colors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColors()
    {
        return $this->hasMany(Colors::className(), ['col_user_id' => 'id']);
    }

    /**
     * Gets query for [[Comments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['com_user_id' => 'id']);
    }

    /**
     * Gets query for [[Likes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Likes::className(), ['lik_user_id' => 'id']);
    }

    /**
     * Gets query for [[ProCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProCategories()
    {
        return $this->hasMany(ProCategories::className(), ['cat_user_id' => 'id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['pro_user_id' => 'id']);
    }

    /**
     * Gets query for [[Profiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profiles::className(), ['pro_user_id' => 'id']);
    }
    public function save($runValidation=true,$attributeNames=NULL){

        $user=new SignupForm();
        $user->username=$this->username;
        $user->email=$this->email;
        $user->password=$this->password;
        $user->item_name=$this->auth_item;
        return $user->signup();

    }
    public function update($runValidation=true,$attributeNames=NULL){

        if (!empty($this->password))
        {
            $this->password_hash=Yii::$app->security->generatePasswordHash($this->password);
        }
        $user_role=AuthAssignment::findOne(['user_id'=>$this->id]);
        if ($user_role)
        {
            $user_role->item_name=$this->auth_item;
            $user_role->update();
        }else{
            $this->id=$user_role->user_id;
            $user=new SignupForm();
            $user->save_assignment();
        }
      return  parent::update();

    }
}
