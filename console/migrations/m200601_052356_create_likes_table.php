<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%likes}}`.
 */
class m200601_052356_create_likes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%likes}}', [
            'lik_id' => $this->primaryKey(),
            'lik_user_id'=>$this->integer(),
            'lik_computer_ip'=>$this->integer(),
            'lik_blag_id'=>$this->integer()->null(),
            'lik_comment_id'=>$this->integer()->null(),
            'lik_like'=>$this->integer(1),
            'lik_created_at'=> $this->integer()->defaultValue(time())->notNull(),
        ]);
        $this->createIndex(
            'idx-likes-user_id',
            'tbl_likes',
            'lik_user_id'
        );
        $this->addForeignKey(
            'fk-likes-user_id',
            'tbl_likes',
            'lik_user_id',
            'tbl_user',
            'id'
//            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-likes-user_id',
            'tbl_likes'
        );
        $this->dropIndex(
            'idx-likes-user_id',
            'tbl_likes'
        );
        $this->dropTable('{{%likes}}');
    }
}
