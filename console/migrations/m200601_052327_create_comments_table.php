<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comments}}`.
 */
class m200601_052327_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comments}}', [
            'com_id' => $this->primaryKey(),
            'com_user_id'=>$this->integer()->null(),
            'com_computer_ip'=>$this->integer()->null(),
            'com_admin_id'=>$this->integer()->null(),//کد تایید کسی که کامنت رو برای نمایش تایید میکنه
            'com_name'=>$this->string()->null(),
            'com_email'=>$this->string()->null(),
            'com_text'=>$this->string(1000),
            'com_type'=>$this->string()->notNull(),//example product
            'com_type_id'=>$this->integer()->notNull(),//example product id 2
            'com_parent_id'=>$this->integer()->null(),//در جواب comment
            'com_meta_type'=>$this->string(),
            'com_like_count'=>$this->integer()->null(),
            'com_view_count'=>$this->integer()->null(),
            'com_comment_count'=>$this->integer()->null(),
            'com_status'=>$this->string(1)->null(),
            'com_created_at'=> $this->integer()->defaultValue(time())->notNull(),
        ]);
        $this->createIndex(
            'idx-comments-user_id',
            'tbl_comments',
            'com_user_id'
        );
        $this->addForeignKey(
            'fk-comments-user_id',
            'tbl_comments',
            'com_user_id',
            'tbl_user',
            'id'
//            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-comments-user_id',
            'tbl_comments'
        );
        $this->dropIndex(
            'idx-comments-user_id',
            'tbl_comments'
        );
        $this->dropTable('{{%comments}}');
    }
}
