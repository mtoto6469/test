<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%themes}}`.
 */
class m200601_043758_create_themes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%themes}}', [
            'thm_id' => $this->primaryKey(),
            'thm_image_id'=>$this->integer(),
            'thm_name'=>$this->string(30)->null(),
            'thm_title'=>$this->string(100),
            'thm_text'=>$this->string(100),
            'thm_position'=>$this->string(150),
            'thm_link'=>$this->string(255),
            'thm_keyword'=>$this->string(50),
            'thm_count_view'=>$this->integer()->defaultValue(0),
            'thm_count_like'=>$this->integer()->defaultValue(0),
            'thm_status'=>$this->string(1)->defaultValue('1'),//off = 0  on = 1  del=2
            'thm_created_at'=>$this->timestamp(),
            'thm_updated_at'=>$this->integer()->notNull(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%themes}}');
    }
}
