<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%categories}}`.
 */
class m200601_052252_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}', [
            'cat_id' => $this->primaryKey(),
            'cat_user_id'=>$this->integer()->notNull(),
            'cat_parent_id'=>$this->integer()->defaultValue(0),
            'cat_image_id'=>$this->integer()->notNull(),
            'cat_type'=>$this->string(1)->notNull(),//1=news  2=articles  3=posts
            'cat_title'=>$this->string(255)->notNull(),
            'cat_descriptions'=>$this->string(255),
            'cat_status' => $this->smallInteger(1)->notNull()->defaultValue(1),//on=1  del=2   off=0
            'slug'=>$this->string(255)->unique()->notNull(),
            'cat_created_at' => $this->integer()->notNull(),
            'cat_updated_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-categories-user_id',
            'tbl_categories',
            'cat_user_id'
        );
        $this->addForeignKey(
            'fk-categories-user_id',
            'tbl_categories',
            'cat_user_id',
            'tbl_user',
            'id'
//            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-categories-user_id',
            'tbl_categories'
        );
        $this->dropIndex(
            'idx-categories-user_id',
            'tbl_categories'
        );
        $this->dropTable('{{%categories}}');
    }
}
