<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%services}}`.
 */
class m200601_052643_create_services_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%services}}', [
            'srv_id' => $this->primaryKey(),
            'srv_user_id'=>$this->integer(),
            'srv_category'=>$this->string(20),
            'srv_title'=>$this->string(),
            'srv_body'=>$this->text(),
            'srv_image_id'=>$this->integer(),
            'srv_location'=>$this->string(255),//address
            'srv_project_time_start'=>$this->integer(),
            'srv_project_time_end'=>$this->integer(),
            'srv_boss'=>$this->string(255),//karfarma
            'srv_keyword'=>$this->string(255),
            'srv_meta_descriptions'=>$this->string(255),
            'srv_meta_title'=>$this->string(255),
            'srv_link'=>$this->string(255),
            'srv_view_count'=>$this->integer()->defaultValue(0),
            'srv_like_count'=>$this->integer()->defaultValue(0),
            'srv_comment_count'=>$this->integer()->defaultValue(0),
            'srv_status' => $this->smallInteger(1)->notNull()->defaultValue(1),//on=1  del=2   off=0
            'srv_lng' => $this->string(3)->notNull()->defaultValue('Fa'),//languages Fa=persian En=english
            'srv_created_at' => $this->integer()->notNull(),
            'srv_updated_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-services-user_id',
            'tbl_services',
            'srv_user_id'
        );
        $this->addForeignKey(
            'fk-services-user_id',
            'tbl_services',
            'srv_user_id',
            'tbl_user',
            'id'
//            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-services-user_id',
            'tbl_services'
        );
        $this->dropIndex(
            'idx-services-user_id',
            'tbl_services'
        );
        $this->dropTable('{{%services}}');
    }
}
