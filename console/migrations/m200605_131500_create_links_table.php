<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%links}}`.
 */
class m200605_131500_create_links_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%links}}', [
            'lnk_id' => $this->primaryKey(),
            'lnk_user_id'=>$this->integer(),
            'lnk_title'=>$this->string(255)->notNull(),
            'lnk_lng'=>$this->string(4)->null(),
            'lnk_created_at'=> $this->integer()->defaultValue(time())->notNull(),
        ]);
        $this->createIndex(
            'idx-links-user_id',
            'tbl_links',
            'lnk_user_id'
        );
        $this->addForeignKey(
            'fk-links-user_id',
            'tbl_links',
            'lnk_user_id',
            'tbl_user',
            'id'
//            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-links-user_id',
            'tbl_links'
        );
        $this->dropIndex(
            'idx-links-user_id',
            'tbl_links'
        );
        $this->dropTable('{{%links}}');
    }
}
