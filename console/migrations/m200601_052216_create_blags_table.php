<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%blags}}`.
 */
class m200601_052216_create_blags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%blags}}', [
            'blg_id' => $this->primaryKey(),
            'blg_user_id'=>$this->integer()->notNull(),
            'blg_category_id'=>$this->integer()->notNull(),
            'blg_image_id'=>$this->integer()->notNull(),
            'blg_title'=>$this->string(255)->unique()->notNull(),
            'blg_body'=>$this->text(),
            'blg_descriptions'=>$this->string(255)->notNull(),
            'blg_laid'=>$this->string(255),
            'blg_keyword'=>$this->string(255),
            'blg_meta_descriptions'=>$this->string(255),
            'blg_meta_title'=>$this->string(255),
            'blg_link'=>$this->string(255),
            'slug'=>$this->string(255)->unique()->notNull(),
            'blg_writer'=>$this->string(255),
            'blg_view_count'=>$this->integer()->defaultValue(0),
            'blg_like_count'=>$this->integer()->defaultValue(0),
            'blg_comment_count'=>$this->integer()->defaultValue(0),
            'blg_status' => $this->smallInteger(1)->notNull()->defaultValue(1),//on=1  del=2   off=0
            'blg_created_at' => $this->integer()->notNull(),
            'blg_updated_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-blags-user-id',
            'tbl_blags',
            'blg_user_id'
        );
        $this->createIndex(
            'idx-blags-category_id',
            'tbl_blags',
            'blg_category_id'
        );
        $this->addForeignKey(
            'fk-blags-user_id',
            'tbl_blags',
            'blg_user_id',
            'tbl_user',
            'id'
//            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-blags-user_id',
            'tbl_blags'
        );
        $this->dropIndex(
            'idx-blags-user_id',
            'tbl_blags'
        );
        $this->dropIndex(
            'idx-blags-category_id',
            'tbl_blags'
        );
        $this->dropTable('{{%blags}}');
    }
}
