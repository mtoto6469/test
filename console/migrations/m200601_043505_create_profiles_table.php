<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profiles}}`.
 */
class m200601_043505_create_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profiles}}', [
            'pro_id' => $this->primaryKey(),
            'pro_user_id'=>$this->integer(),
            'pro_fullName'=>$this->string(50),
            'pro_en_fullName'=>$this->string(50),
            'pro_image_id'=>$this->integer(),
            'pro_position'=>$this->string(30),
            'pro_en_position'=>$this->string(30),
            'pro_degree'=>$this->string(30),
            'pro_en_degree'=>$this->string(30),
            'pro_birthday'=>$this->date(),
            'pro_gender'=>$this->string(1),
            'pro_phone'=>$this->string(11),
            'pro_mobile'=>$this->string(11),
            'pro_nationalCode'=>$this->string(11),
            'pro_postalCode'=>$this->string(11),
            'pro_province'=>$this->string(20),
            'pro_city'=>$this->string(30),
            'pro_en_address'=>$this->string(200),
            'pro_address'=>$this->string(200),
            'pro_descriptions'=>$this->string(255),
            'pro_en_descriptions'=>$this->string(255),
            'pro_biography'=>$this->text(),
            'pro_en_biography'=>$this->text(),
            'pro_valet'=>$this->integer(),
            'pro_status'=>$this->string(1)->defaultValue('1'),//off = 0  on = 1
            'pro_created_at'=>$this->timestamp(),
            'pro_updated_at'=>$this->integer(),
        ]);
        $this->createIndex(
            'idx-profiles-user_id',
            'tbl_profiles',
            'pro_user_id'
        );
        $this->addForeignKey(
            'fk-profiles-user-id',
            'tbl_profiles',
            'pro_user_id',
            'tbl_user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-profiles-user_id',
            'tbl_profiles'
        );
        $this->dropIndex(
            'idx-profiles-user_id',
            'tbl_profiles'
        );
        $this->dropTable('{{%profiles}}');
    }
}
