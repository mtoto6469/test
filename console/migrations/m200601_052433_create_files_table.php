<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%files}}`.
 */
class m200601_052433_create_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%files}}', [
            'img_id' => $this->primaryKey(),
            'img_user_id'=>$this->integer(),
            'img_type'=>$this->string(10),
            'img_type_id'=>$this->integer(),
            'img_enable'=>$this->string(),
            'img_image'=>$this->string(255),
            'img_name'=>$this->string(255),
            'img_link'=>$this->string(255),
            'slug'=>$this->string(255)->unique()->notNull(),
            'img_view_count'=>$this->integer()->defaultValue(0),
            'img_like_count'=>$this->integer()->defaultValue(0),
            'img_comment_count'=>$this->integer()->defaultValue(0),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%files}}');
    }
}
