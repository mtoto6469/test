var swiper = new Swiper('.swiper-container', {
  autoplay: {
    delay: 2000,
    disableOnInteraction: false,
  },
  effect: 'coverflow',
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: 'auto',
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows : true,
  },
  pagination: {
    el: '.swiper-pagination',
  },
});

$(window).scroll(function () {
  if ($(window).scrollTop() > 200) {
    $('#scroll_fix').show();

  } else {
    $('#scroll_fix').hide();
  }
});
$("#scroll_fix").click(function () {
  $('html').animate({scrollTop: 0}, 1000)
});

$(window).scroll(function () {
  if ($(window).marginRight() > 200) {
    $('.responsive_menu').show();
  } else {
    $('.responsive_menu').hide();
  }
});

var owl = $('.owl-carousel');
owl.owlCarousel({
  items:4,
  loop:true,
  margin:10,
  autoplay:true,
  autoplayTimeout:1000,
  autoplayHoverPause:true
});
$('.play').on('click',function(){
  owl.trigger('play.owl.autoplay',[1000])
});
$('.stop').on('click',function(){
  owl.trigger('stop.owl.autoplay')
});


$(document).ready(function () {



  $(".menu_drop1").click(function () {
    $(".drop_down1").slideToggle("slow");
    $(".drop_down2").slideUp("fast")
    $(".drop_down3").slideUp("fast")
  });


  $(".menu_drop2").click(function () {
    $(".drop_down2").slideToggle("slow");
    $(".drop_down1").slideUp("fast")
    $(".drop_down3").slideUp("fast")

  });

  $(".menu_drop3").click(function () {
    $(".drop_down3").slideToggle("slow");
    $(".drop_down1").slideUp("fast")
    $(".drop_down2").slideUp("fast")

  });

  $(".menu_drop4").click(function () {
    $(".drop_down4").slideDown("slow");
    $(".drop_down5").slideUp("fast")
    $(".drop_down6").slideUp("fast")
  });


  $(".menu_drop5").click(function () {
    $(".drop_down5").slideToggle("slow");
    $(".drop_down4").slideUp("fast")
    $(".drop_down6").slideUp("fast")

  });

  $(".menu_drop6").click(function () {
    $(".drop_down6").slideToggle("slow");
    $(".drop_down4").slideUp("fast")
    $(".drop_down5").slideUp("fast")

  });







  $(".menu_res_drop2").click(function () {
    $(".drop_res_down2").slideToggle("slow");
    $(".drop_res_down1").slideUp("fast")

  });

  $(".menu_res_drop1").click(function () {
    $(".drop_res_down1").slideToggle("slow");
    $(".drop_res_down2").slideUp("fast")
  });




  $("#menu_icon").click(function () {
    $(".responsive_menu").slideToggle("slow");
  });


});


