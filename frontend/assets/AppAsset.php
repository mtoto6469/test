<?php

namespace frontend\assets;

use yii;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        'css/swiper.min.css',
        'css/bootstrap.min.css',
        'css/fontawesome-free-5.8.2-web/css/all.css',
        'css/slider.css',
        'css/nav.css',
        'css/aos.css',
        'css/owlcss.css',
        'css/owl.carousel.min.css',
        'css/owl.theme.default.min.css',
        'css/main.css'
    ];
    public $js = [
        'js/jquery.3.3.1.min.js',
        'js/owl.carousel.min.js',
        'js/swiper.min.js',
        'js/main.js',
        'js/aos.js',
//        'js/'
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
    public function init()
    {
        if (Yii::$app->language=='en' || Yii::$app->language=='en-EN'){
            array_push($this->css,'css/english_main.css');
        }
        parent::init();
    }
}
