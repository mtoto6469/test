<?php

use common\models\Services;
use yii\helpers\Url;

?>
<div class="container-fluid english">
    <!--header-->
    <div class="row header d-flex justify-content-around">
        <div class="col-xl-5 col-lg-5  col-md-5 col-sm-5 col-12 d-flex align-items-center" id="phone">
            <span class="phone">02144268214</span>
            <span class="phone"><?= Yii::t('app','Fax') ?></span>
            <span class="phone">02144268215</span>
            <span class="phone"><?= Yii::t('app','Phone') ?></span>
        </div>
        <div class="col-xl-1 col-lg-1  col-md-1 col-sm-1 col-1 d-flex align-items-center" id="phone">
            <span class="phone">
                <?php
                echo '<div style="float: right;padding-top: 15px;color:white">
    <a style="padding-right: 10px;padding-left: 30px" href="'. Url::base().'fa'.'">'
                    .'FA'
                    . '</a>'.
                    '<a href="'. Url::base().'en'.'">'
                    .'EN'
                    . '</a></div>';
                ?>
            </span>
        </div>
        <div class="col-xl-5 col-lg-5  col-md-5 col-sm-5 col-5 d-flex align-items-center justify-content-end social_media">
            <a href="#">
                <i class="fab fa-telegram socialnetwork"></i>
            </a>
            <a href="#">
                <i class="fab fa-instagram socialnetwork"></i>
            </a>
            <a href="#">
                <i class="fab fa-twitter socialnetwork"></i>
            </a>
            <a href="#">
                <i class="fab fa-linkedin socialnetwork"></i>
            </a>
        </div>
    </div>
    <!--container-->
    <div class="row d-flex justify-content-around align-items-center menu_total">
        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-7 noPadding">
            <div class="" style="width: 50%;height: 130px">
                <img src="<?= Yii::$app->urlManager->hostInfo  ?>/frontend/web/images/logo007.png" width="100%" height="100%">
            </div>

        </div>
        <div class="col-xl-7 col-lg-8 col-md-9 col-sm-4  col-4  d-flex justify-content-end align-items-center noPadding">
            <ul class="menu">
                <li>
                    <a href="<?=Yii::$app->request->baseUrl .'/site' ?>"><?= Yii::t('app','Home') ?></a>
                </li>
                <li class="menu_drop1">
                    <a href="#"><?= Yii::t('app','Projects') ?></a>
                    <i class='fas fa-caret-down menu_icon'></i>
                </li>
                <li class="menu_drop2">
                    <a href="#"><?= Yii::t('app','Structure And Organization') ?></a>
                    <i class='fas fa-caret-down menu_icon'></i>
                </li>
                <li>
                    <a href="#"><?=  Yii::t('app','Educational Content') ?></a>
                </li>
                <li>
                    <a href="#"><?= Yii::t('app','Partners') ?></a>
                </li>
                <li>
                    <a href="#"><?= Yii::t('app','News') ?></a>
                </li>
                <li class="menu_drop3">
                    <a href="#"><?= Yii::t('app','About Us') ?></a>
                    <i class='fas fa-caret-down menu_icon'></i>
                </li>
                <li>
                    <a href="<?= Yii::$app->request->baseUrl .'/about' ?>"><?= Yii::t('app','Contact') ?></a>
                </li>
            </ul>
            <div class="drop_down1 drop_p">
                <?php
                $services=Services::find()->select(['srv_title','srv_id'])->andWhere(['srv_lng'=>Yii::$app->language])->all();
                foreach ($services as $service){?>
                    <a href="<?=Yii::$app->request->baseUrl.'/project/'.$service->srv_id ?>"><p><?= $service->srv_title; ?></p></a>
              <?php  }
                ?>
            </div>
            <div class="drop_down2 drop_p">

                <a href=""><p><?= Yii::t('app','Organization Chart')  ?></p></a>
                <a href=""><p><?= Yii::t('app','Strategic Plan')  ?></p></a>
            </div>
            <div class="drop_down3 drop_p">
                <p>نمودار سازمانی</p>
                <p>برنامه استراتژیک</p>
            </div>
            <i class="fa fa-bars" id="menu_icon"></i>
        </div>
    </div>
    <div class="responsive_menu">

        <ul class="menu_res">
            <li>
                <span></span>
                <a href="#"><?= Yii::t('app','Home') ?></a>
            </li>
            <li class="menu_res_drop1">
                <i class='fas fa-plus menu_icon'></i>
                <a href="#"><?= Yii::t('app','Projects') ?></a>
            </li>
            <div class="drop_res_down1">
            </div>
            <li class="menu_res_drop2">
                <i class='fas fa-plus menu_icon'></i>
                <a href="#"><?= Yii::t('app','Structure And Organization') ?></a>
            </li>
            <div class="drop_res_down2">
                <li><a href="#">پروژه ها</a></li>
                <li><a href="#">پروژه ها</a></li>
            </div>
            <li>
                <span></span>
                <a href="#"><?= Yii::t('app','Educational Content') ?></a>
            </li>
            <li>
                <span></span>
                <a href="#"><?= Yii::t('app','Partners') ?></a>
            </li>
            <li>
                <span></span>
                <a href="#"><?= Yii::t('app','News') ?></a>
            </li>
            <li>
                <i class='fas fa-plus menu_icon'></i>
                <a href="#"><?= Yii::t('app','About Us') ?></a>
            </li>
            <li id="border_buttom">
                <span></span>
                <a href="#"><?= Yii::t('app','Contact') ?></a>
            </li>
        </ul>


    </div>