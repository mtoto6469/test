<?php

use common\models\Links;
use common\models\Pages;
use common\models\Services;

foreach (Pages::find()->all() as $value){
    $values[$value->key] = $value->body;
}

?>

<!--footer-->
<div class="row d-flex justify-content-center align-items-center footer">
    <div class="col-xl-9 d-flex justify-content-around flex-wrap" style="margin-top: 20px">
        <div class="col-xl-4 col-md-10 col-sm-12">

            <div class="cantactus" style="width: 130px;height: 130px">
                <img src="<?= Yii::$app->urlManager->hostInfo  ?>/frontend/web/images/logo007.png" width="100%" height="100%">
            </div><br>
            <p class="cantactus_text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است</p>
            <div class="social_footer">
                <a href="#">
                    <i class="fab fa-telegram socialnetwork"></i>
                </a>
                <a href="#">
                    <i class="fab fa-instagram socialnetwork"></i>
                </a>
                <a href="#">
                    <i class="fab fa-twitter socialnetwork"></i>
                </a>
                <a href="#">
                    <i class="fab fa-linkedin socialnetwork"></i>
                </a>
            </div>

        </div>
        <div class="col-xl-4 col-md-10 col-sm-12 d-flex justify-content-start align-items-center text-right f_margin_top">
            <div class="link">
                <p><?= Yii::t('app','Keep In Touch') ?></p>
                <p>
                    <i class="fas fa-map-marker"></i>
                    <span><?= Yii::$app->language=='fa' ?  $values[Pages::ITEM_ADDRESS]:  $values[Pages::ITEM_EN_ADDRESS] ?></span>

                </p>
                <p>
                    <i class="fas fa-envelope"></i>
                    <span><?= Yii::t('app','Iranian Society of Consulting Engineers') ?></span>
                </p>
            </div>
        </div>
        <div class="col-xl-3  col-md-10 col-sm-12 d-flex justify-content-start align-items-center text-right">
            <div class="link">
                <p><?= Yii::t('app','Related Links') ?></p>
                <?php $links=Links::find()->where(['lnk_lng'=>Yii::$app->language])->all();
                if ($links){
                foreach ($links as $link){ ?>
                    <p>
                        <a href="<?= $link->lnk_link ?>"><?= $link->lnk_title ?></a>
                    </p>
                <?php } } ?>

                <div></div>
            </div>

        </div>

    </div>
</div>
<!--scroll-->
<div class="banner-bridge-center" id="scroll_fix">
    <a class="scroll-button button scroll-next">
        <i class="fa fa-angle-up"></i>
    </a>
</div>
<!--topmenu-->
<div class="row d-flex justify-content-around align-items-center menu_total" id="menu_total">
    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-7 noPadding">
        <div class="" style="width: 50%;height: 130px">
            <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/logo007.png " width="100%" height="100%">
        </div>

    </div>
    <div class="col-xl-7 col-lg-8 col-md-9 col-sm-4  col-4  d-flex justify-content-end align-items-center noPadding">
        <ul class="menu">
            <li>
                <a href="<?=Yii::$app->request->baseUrl .'/site' ?>"><?= Yii::t('app','Home') ?></a>
            </li>
            <li class="menu_drop4">
                <a href="<?=Yii::$app->request->baseUrl .'/site' ?>"><?= Yii::t('app','Projects') ?></a>
                <i class='fas fa-caret-down menu_icon'></i>
            </li>
            <li class="menu_drop5">
                <a href="#"><?= Yii::t('app','Structure And Organization') ?></a>
                <i class='fas fa-caret-down menu_icon'></i>
            </li>
            <li>
                <a href="#"><?=  Yii::t('app','Educational Content') ?></a>
            </li>
            <li>
                <a href="#"><?= Yii::t('app','Partners') ?></a>
            </li>
            <li>
                <a href="#"><?= Yii::t('app','News') ?></a>
            </li>
            <li class="menu_drop6">
                <a href="<?= Yii::$app->request->baseUrl .'/about' ?>"><?= Yii::t('app','About Us') ?></a>
                <i class='fas fa-caret-down menu_icon'></i>
            </li>
            <li>
                <a href="#"><?= Yii::t('app','Contact') ?></a>
            </li>
        </ul>
        <div class="drop_down4 drop_p">
            <?php
            $services=Services::find()->select(['srv_title','srv_id'])->all();
            foreach ($services as $service){?>
                <a href="<?=Yii::$app->request->baseUrl.'/project/'.$service->srv_id ?>"><p><?= $service->srv_title ?></p></a>
            <?php  }
            ?>
        </div>
        <div class="drop_down5 drop_p">
            <a href=""><p><?= Yii::t('app','Organization Chart')  ?></p></a>
            <a href=""><p><?= Yii::t('app','Strategic Plan')  ?></p></a>
        </div>
        <div class="drop_down6 drop_p">
            <p>نمودار سازمانی</p>
            <p>برنامه استراتژیک</p>
        </div>
        <i class="fa fa-bars"></i>
    </div>
</div>


</div>


<script>
    AOS.init();
</script>

<script>
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 180 || document.documentElement.scrollTop > 180) {
            document.getElementById("menu_total").style.top = "0";
        } else {
            document.getElementById("menu_total").style.top = "-150px";
        }
    }
</script>


