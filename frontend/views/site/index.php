<?php
$slid=Yii::$app->urlManager->hostInfo .'/upload/sliders/'
?>

<?php if ($sliders){ ?>
<!--slider-->
<div class="row slide">
    <div class="slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($sliders as $slider){ ?>
                <div class="swiper-slide">
                    <img src="<?= $slid.$slider->img_image ?>" alt="<?= $slider->img_image ?>" style="width: 100%;height: 100%">
                </div>
                <?php } ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
<?php } ?>
<!--navigation-->
<div class="nav">
    <h2><?= Yii::t('app','Some Portfolios') ?></h2>
</div>
<div class="line_total">
    <span class="line"></span>
</div>
<div class="row" style="margin: 0 auto">
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="box5">
            <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/imagenav.jpeg" alt="">
            <ul class="icon">
                <li><a href="#" class="fa fa-link"></a></li>
            </ul>
            <div class="box-content">
                <h3 class="title"><?= Yii::t('app','To See More') ?></h3>
                <span class="post"><?= Yii::t('app','Click On The Link') ?></span>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="box5">
            <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/imagenav.jpeg" alt="">
            <ul class="icon">
                <li><a href="#" class="fa fa-link"></a></li>
            </ul>
            <div class="box-content">
                <h3 class="title"><?= Yii::t('app','To See More') ?></h3>
                <span class="post"><?= Yii::t('app','Click On The Link') ?></span>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="box5">
            <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/imagenav.jpeg" alt="">
            <ul class="icon">
                <li><a href="#" class="fa fa-link"></a></li>
            </ul>
            <div class="box-content">
                <h3 class="title"><?= Yii::t('app','To See More') ?></h3>
                <span class="post"><?= Yii::t('app','Click On The Link') ?></span>
            </div>
        </div>
    </div>
</div>
<!--aboutus-->
<div class="row back_about_us d-flex align-items-center justify-content-around">
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6  col-12 box_about_us_left d-flex justify-content-end">
           <span class="box_about_us_text">
           لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از
           طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که
           لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود
           ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده،
           </span>

    </div>
    <div class="col-xl-5 col-lg-5 col-md-6 col-sm-6  col-12 d-flex justify-content-center align-items-center box_about_us_right" style="background-color: wheat">
        <div>
            <p class="box_about_us_title"><?= Yii::t('app','From Service And Counseling')  ?></p>
            <p class="box_about_us_title"><?= Yii::t('app','We Are Happy For You') ?></p>
        </div>
    </div>
    <!--<div class="owl-carousel owl-theme" id="slider2">-->
    <!--<div class="item"><h4>1</h4></div>-->
    <!--<div class="item"><h4>2</h4></div>-->
    <!--<div class="item"><h4>3</h4></div>-->
    <!--<div class="item"><h4>4</h4></div>-->
    <!--<div class="item"><h4>5</h4></div>-->
    <!--<div class="item"><h4>6</h4></div>-->
    <!--<div class="item"><h4>7</h4></div>-->
    <!--<div class="item"><h4>8</h4></div>-->
    <!--<div class="item"><h4>9</h4></div>-->
    <!--<div class="item"><h4>10</h4></div>-->
    <!--<div class="item"><h4>11</h4></div>-->
    <!--<div class="item"><h4>12</h4></div>-->
    <!--</div>-->

</div>
<!--news-->
<div class="row d-flex justify-content-center" style="background: #eff4f5">
    <div class="news_tittle">
        <h2><?= Yii::t('app','News') ?></h2>
        <div class="line_total">
            <span class="line"></span>
        </div>
    </div>

    <div class="col-sm-12 d-flex justify-content-around flex-wrap">
        <div class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-12 d-flex justify-content-around align-items-center news_tottall" >
            <div class="news">
                <div class="image_news">
                    <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/slider6.jpg" width="100%" height="100%">
                </div>
                <div class="news_text">
                    <h6>از نظر عینی گسترده است</h6>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                    <p class="btn_news_text">
                        <a href="#"><?= Yii::t('app','Read More') ?></a>
                        <i class='fas fa-arrow-left d'></i>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-12 d-flex justify-content-around align-items-center news_tottall" >
            <div class="news">
                <div class="image_news">
                    <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/slider6.jpg" width="100%" height="100%">
                </div>
                <div class="news_text">
                    <h6>از نظر عینی گسترده است</h6>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                    <p class="btn_news_text">
                        <a href="#"><?= Yii::t('app','Read More') ?></a>
                        <i class='fas fa-arrow-left d'></i>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-12 d-flex justify-content-around align-items-center news_tottall">
            <div class="news">
                <div class="image_news">
                    <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/slider6.jpg" width="100%" height="100%">
                </div>
                <div class="news_text">
                    <h6>از نظر عینی گسترده است</h6>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                    <p class="btn_news_text">
                        <a href="#"><?= Yii::t('app','Read More') ?></a>
                        <i class='fas fa-arrow-left d'></i>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--aboutteam-->
<div class="row  d-flex justify-content-around align-items-center about_team">
    <div class="aboat_team_tittle">
        <p><?= Yii::t('app','Our Team') ?></p>
        <h1><?= Yii::t('app','Giti Team Members') ?></h1>
        <div class="line_total">
            <span class="line"></span>
        </div>

        <div class="about_team_total_box flex-wrap">
            <div class="col-xl-3 col-lg-5 col-md-6 col-sm-6 col-12">
                <div class="about_team_box">
                    <div class="about_team_profile">
                        <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/image4.png" class="profile_image">
                        <p class="profile_text profile_margin">مهندس سید حسین غفاری</p>
                        <p class="profile_text">مدیر عامل و عضو هیئت مدیره</p>
                        <p class="profile_text">کارشناس ارشد مهندسی و مدیریت ساخت</p>
                        <p class="profile_text">
                            <a href="#"><?= Yii::t('app','More') ?>  </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-5 col-md-6 col-sm-6 col-12">
                <div class="about_team_box">
                    <div class="about_team_profile">
                        <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/image4.png" class="profile_image">
                        <p class="profile_text  profile_margin">مهندس منوچهر خزائل</p>
                        <p class="profile_text">رئیس هیئت مدیره</p>
                        <p class="profile_text">کارشناس تاسیسات مکانیک سیالات</p>
                        <p class="profile_text">
                            <a href="#"><?= Yii::t('app','More') ?>  </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-5 col-md-6 col-sm-6 col-12">
                <div class="about_team_box">
                    <div class="about_team_profile">
                        <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/image4.png" class="profile_image">
                        <p class="profile_text  profile_margin">مهندس محمدرضا جوانروح</p>
                        <p class="profile_text">عضو هیئت مدیره</p>
                        <p class="profile_text">کارشناس ارشد سازه</p>
                        <p class="profile_text">
                            <a href="#"><?= Yii::t('app','More') ?> </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-5 col-md-6 col-sm-6 col-12">
                <div class="about_team_box">
                    <div class="about_team_profile">
                        <img src="<?= Yii::$app->urlManager->hostInfo ?>/frontend/web/images/image4.png" class="profile_image">
                        <p class="profile_text  profile_margin">مهندس احمد خجسته</p>
                        <p class="profile_text">عضو هیئت مدیره</p>
                        <p class="profile_text">کارشناس معماری</p>
                        <p class="profile_text">
                            <a href="#"><?= Yii::t('app','More') ?> </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


