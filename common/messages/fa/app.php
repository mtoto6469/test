<?php
return[
    'update'=>'ویرایش',
    'create'=>'ایجاد',
    'delete'=>'حذف',
    'Exit'=>'خروج',
    'Save'=>'ثبت',
    //main
    'Public Information'=>'اطلاعات عمومی',
    'Information'=>'اطلاعات',
    'Image'=>'عکس',
    'Users'=>'کاربران',
    'Create New User'=>'ثبت کاربر جدید',
    'Manege User'=>'مدیریت کاربران',
    'Create New Brand'=>'ثبت برند جدید',
    'All Brands'=>'مدیریت برند ها',
    'Create New Category'=>'ثبت دسته جدید',
    'All Categories'=>'مدیریت دسته بندی ها',
    'Create New Product'=>'ثبت محصول جدید',
    'All Products'=>'مدیریت محصولات',
    'Blags'=>'بلاگ',
    'Create New Blag'=>'ثبت بلاگ جدید',
    'All Blags'=>'مدیریت بلاگ ها',
    'Sliders'=>'اسلایدر',
    'Create New Slid'=>'ثبت اسلاید جدید',
    'All Slider'=>'مدیریت سلایدر ',
    'Users Comments'=>'کامنت کاربران',
    'All Comments'=>'مدیریت کامنت کاربران',
    'Services'=>'سرویس ها',
    'Create New Service In Persian Page'=>'ثبت سرویس جدید در صقحه فارسی',
    'Create New service In English Page'=>'ثبت سرویس جدید در صقحه انگلیسی',
    'Links'=>'لینک ها',
    'Create New Fa Blag'=>'ثبت بلاگ جدید در صقحه فارسی',
    'Create New En Blag'=>'ثبت بلاگ جدید در صقحه انگلیسی',
    ''=>'',
    //user
    'Create User'=>'ثبت کاربر',
    'Username'=>'نام کاربری',
    'auth_key'=>'auth_key',
    'Password'=>'رمز عبور',
    'password_hash'=>'password_hash',
    'password_reset_token'=>'password_reset_token',
    'email'=>'ایمیل',
    'status'=>'وضعیت',
    'verification_token'=>'verification_token',
    'All Links'=>'لینک ها',
    'role'=>'نقش کاربر',
//    'Strategic Program'=>'',
//
    //profile
    'Profiles'=>'پروفایل کاربران',
    'Pro User Username'=>'نام کاربری',
    'Pro User ID'=>'نام کاربری',
    'Pro Full Name'=>'نام',
    'Pro En Full Name'=>'نام لاتین',
    'Pro Image ID'=>'عکس کاربران',
    'Pro Birthday'=>'تاریخ تولد',
    'Pro Gender'=>'جنسیت',
    'Pro Phone'=>'تلفن',
    'Pro Mobile'=>'موبایل',
    'Pro National Code'=>'کدملی',
    'Pro Postal Code'=>'کدپستی',
    'Pro Province'=>'استان',
    'Pro City'=>'شهر',
    'Pro Address'=>'آدرس',
    'Pro En Address'=>'آدرس لاتین',
    'Pro Descriptions'=>'توضیحات',
    'Pro En Descriptions'=>'توضیحات لاتین',
    'Pro Biography'=>'بیوگرافی',
    'Pro En Biography'=>'بیوگرافی لاتین',
    'Pro Status'=>'وضعیت',
    'Pro Created At'=>'تاریخ ثبت',
    'Pro Updated At'=>'تاریخ ویرایش',
    'Male'=>'مرد',
    'Female'=>'زن',
    'Pro Position'=>'عنوان شغلی',
    'Pro En Position'=>'عنوان شغلی لاتین',
    'Pro Degree'=>'مدرک تحصیلی',
    'Pro En Degree'=>'مدرک تحصیلی لاتین',
    'birthday'=>'تاریخ تولد',

    //category and pro category
    'Create Categories'=>'ثبت دسته',
    'Categories'=>'دسته بندی ها',
    'Cat Id'=>'',
    'Cat User Id'=>'نام کاربری',
    'Cat Parent Id'=>'دسته والد',
    'Cat Type'=>'نوع دسته',
    'Cat Title'=>'عنوان',
    'Cat En Title'=>'عنوان لاتین',
    'Cat Descriptions'=>'توضیحات',
    'Cat En Descriptions'=>'توضیحات لاتین',
    'Cat Image Id'=>'عکس',
    'Cat Status'=>'وضعیت',
    'Cat Created At'=>'تاریخ ثبت',
    'Cat Updated At'=>'تاریخ ویرایش',

    //blog
    'Create Blags'=>'ثبت بلاگ',
    'Blg Id'=>'',
    'Blg User Id'=>'نام کاربری',
    'Blg User Username'=>'ثیت کننده',
    'Blg Category Id'=>'نام دسته بندی',
    'Blg Category ID'=>'نام دسته بندی',
    'Blg Title'=>'عنوان',
    'Blg EN Title'=>'عنوان لاتین',
    'Blg Body'=>'متن',
    'Blg En Body'=>'متن لاتین',
    'Blg Descriptions'=>'توضیحات',
    'Blg En Descriptions'=>'توضیحات لاتین',
    'Blg Laid'=>'زبان',
//    'Blg laid'=>'توضیح کوتاه',
    'Blg Image Id'=>'عکس',
    'Blg Keyword'=>'کلمات کلیدی',
    'Blg En Keyword'=>'کلمات کلیدی لاتین',
    'Blg Meta Descriptions'=>'سئو توضیحات',
    'Blg En Meta Descriptions'=>'سئو توضیحات لاتین',
    'Blg Meta Title'=>'سئو عنوان',
    'Blg En Meta Title'=>'سئو عنوان لاتین',
    'Blg Link'=>'لینک',
    'Slug '=>'Slug',
    'Blg Writer'=>'نویسنده',
    'Blg View Count'=>'تعداد باردیدها',
    'Blg Like Count'=>'تعداد لایک ها',
    'Blg Comment Count'=>'تعداد کامنت ها',
    'Blg Status'=>'وضعیت',
    'Blg Created At'=>'تاریخ ثبت',
    'Blg Updated At'=>'تاریخ ویرایش',
    //brands
    'Create Brands'=>'',
    'Brands'=>'',
    'Update Brands: {name}'=>'',
    'brn_id'=>'',
    'brn_user_id'=>'',
    'brn_title'=>'',
    'brn_descriptions'=>'',
    'brn_image'=>'',
    'brn_status'=>'',
    'brn_created_at'=>'',
    'brn_updated_at'=>'',
    //colors
    'Colors'=>'',
    'Update Colors: {name}'=>'',
    'Create Colors'=>'',
    'col_id'=>'',
    'col_user_id'=>'',
    'col_title'=>'',
    'col_descriptions'=>'',
    'col_status'=>'',
    'col_created_at'=>'',
    'col_updated_at'=>'',
    //product
    'Create Products'=>'',
    'Products'=>'',
    'Update Products: {name}'=>'',
    'pro_id'=>'',
    'pro_user_id'=>'',
    'pro_category_id'=>'',
    'pro_brand_id'=>'',
    'pro_color_id'=>'',
    'pro_size_id'=>'',
    'pro_supplier_id'=>'',
    'pro_title'=>'',
    'pro_price'=>'',
    'pro_sale_off'=>'',
    'pro_body'=>'',
    'pro_descriptions'=>'',
    'pro_image'=>'',
    'pro_keyword'=>'',
    'pro_meta_descriptions'=>'',
    'pro_meta_title'=>'',
    'pro_link'=>'',
    'pro_writer'=>'',
    'pro_product_count'=>'',
    'pro_view_count'=>'',
    'pro_like_count'=>'',
    'pro_comment_count'=>'',
    'pro_status'=>'',
    'pro_begin_date_sale_off'=>'',
    'pro_end_date_sale_off'=>'',
    'pro_created_at'=>'',
    'pro_updated_at'=>'',
    //pages
    'Logo'=>'',
    'EN Logo'=>'',
    'LabelTitle'=>'عنوان سایت',
    'En LabelTitle'=>'عنوان انگلیسی سایت ',
    'Seo Title'=>'سو عنوان',
    'Seo En Title'=>'سئو عنوان انگلیسی',
    'Description'=>'توضیحات',
    'En Description'=>'توضیحات',
    'Seo Description'=>'سو توضیحات',
    'Seo En Description'=>'سو توضیحات',
    'Phone'=>'تلفن',
    'Email'=>'ایمیل',
    'Instagram'=>'اینستاگرام',
    'Telegram'=>'تلگرام',
    'Address'=>'آدرس',
    'En Address'=>'آدرس',
    'About'=>'درباره ما',
    'En About'=>'درباره ما',
    'Rule'=>'قوانین و مقررات',
    'En Rule'=>'قوانین و مقررات',
    'Key Word'=>'کلمات کلیدی',
    'En Key Word'=>'کلمات کلیدی',

    //service
    'Srv ID'=>'کد',
    'Srv User ID'=>'کاربر',
    'Srv Category'=>'دسته بندی',
    'Srv Title'=>'عنوان',
    'Srv Body'=>'متن',
    'Srv Image ID'=>'فایل',
    'Srv Location'=>'مکان',
    'Srv Project Time Start'=>'تاریخ شروع پروژه',
    'Srv Project Time End'=>'تاریخ اتمام پروژه',
    'Srv Boss'=>'کارفرما',
    'Srv Keyword'=>'کلمات کلیدی',
    'Srv Meta Description'=>'عنوان سئو',
    'Srv Link'=>'لینک',
    'Srv View Count'=>'تعداد بازدید',
    'Srv Like Count'=>'تعداد لایک',
    'Srv Comment Count'=>'تعداد کامنت',
    'Srv Status'=>'وضعیت',
    'Srv Created At'=>'تاریخ ثبت',
    'Srv Updated At'=>'تاریخ ویرایش',

    //links
    ''=>'',
    'Lnk Title'=>'عنوان سایت',
    'Lnk Link'=>'آدرس',
    'Lnk Lng'=>'زبان',
    //slider
    'Img Image'=>'اسلاید',
    'Img ID'=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    //frontend blags module
    'More'=>'بیشتر ...',
    'not found any category'=>'هیچ دسته ای پیدا نشد.',
    'not found any blag'=>'هیچ بلاگ یا مقاله ای پیدا نشد.',
    'user comments'=>'نظرات کاربران',
    'Home'=>'صفحه اصلی',
    'Projects'=>'پروژه ها',
    'Structure And Organization'=>'ساختار و سازمان',
    'Educational Content'=>'مطالب آموزشی',
    'Partners'=>'همکاران',
    'News'=>'اخبار',
    'Organization Chart'=>'نمودار سازمانی',
    'Strategic Plan'=>'برنامه استراتژیک',
    'Iranian Society of Consulting Engineers'=>'جامعه مهندسان مشاور ایران',
    'Fax'=>'نمابر',
    'About Us'=>'درباره ما',
    'Contact'=>'تماس با ما',
    'Related Links'=>'لینک های مرتبط',
    'Email'=>'ایمیل',
    'Keep In Touch'=>'در تماس باشید',
    'From Service And Counseling'=>'از خدمت رسانی و ارائه مشاوره',
    'We Are Happy For You'=>'.به شما خرسندیم',
    'Some Portfolios'=>'برخی از نمونه کارا',
    'To See More'=>'برای مشاهده بیشتر',
    'Click On The Link'=>'بر روی لینک کلیک کنید',
    'Read More'=>'بیشتر بخوانید ',
    'More'=>'... بیشتر',
    'Our Team'=>'تیم ما',
    'Giti Team Members'=>'اعضای تیم گیتی',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
]
    ?>
