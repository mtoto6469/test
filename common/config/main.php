<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
//                    'sourceLanguage' => 'en-US',
                    'basePath' => '@common/messages'
                ],
            ],
        ],
//        'urlManager' => [
//            'baseUrl'=>'',
//            'class' => 'common\components\ZurlManager',//for multilingual
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
////            'enableStrictParsing'=>true,
//            'rules' => [
//                '<language:\w+>/' => 'site/index',//for multilingual
//                '<language:\w+>/<action>' => 'site/<action>',//for multilingual
//                '<language:\w+>/<controller>/<action>' => '<controller>/<action>',//for multilingual
//                '<language:\w+>/<module>/<controller>/<action>' => '<module>/<controller>/<action>',//for multilingual
////
//            ],
//        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
//        'session'=>[
//            'timeout'=>60*60*24*14,//for 2 weeks
//        ],
        'message'=>[
            'class'=>'common\components\MessageComponent',
        ],
        'main'=>[
            'class'=>'common\components\MiriamMain',
        ],
        'nationalCode'=>[
            'class'=>'common\components\NationalCode',
        ],
        'jdf'=>[
            'class'=>'common\components\jdf',
        ],

    ],
];
