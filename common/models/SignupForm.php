<?php
namespace common\models;

use backend\models\AuthAssignment;
use common\models\User;
use Yii;
use yii\base\Model;
//use function foo\func;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $id;
    public $item_name;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return  null;
//            var_dump($this->errors);exit;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status=10;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        if( $user->save())
        {
            $this->id=$user->id;
            $profile=new Profiles();
            $profile->pro_user_id=$this->id;
            if ($profile->save()){
                if ( $this->save_assignment() == true)
                {
                    return $user->id;
                }
            }else{
                $user->delete();
            }


        }else{
            var_dump($user->errors);exit;
        }

    }

    public function save_assignment(){
        $assignment=new AuthAssignment();
        $assignment->user_id=$this->id;
        $assignment->item_name=$this->item_name;
        $assignment->created_at=time();
        if ($assignment->save())
        {
            return true;
        }else{

            $user=User::findOne($this->id);
            $profile=Profiles::findOne(['pro_user_id'=>$this->id]);
            $profile->delete();
            $user->delete();
            return false;
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
