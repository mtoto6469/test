<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_services".
 *
 * @property int $srv_id
 * @property int|null $srv_user_id
 * @property string|null $srv_category
 * @property string|null $srv_title
 * @property string|null $srv_body
 * @property int|null $srv_image_id
 * @property string|null $srv_location
 * @property int|null $srv_project_time_start
 * @property int|null $srv_project_time_end
 * @property string|null $srv_boss
 * @property string|null $srv_keyword
 * @property string|null $srv_meta_descriptions
 * @property string|null $srv_meta_title
 * @property string|null $srv_link
 * @property int|null $srv_view_count
 * @property int|null $srv_like_count
 * @property int|null $srv_comment_count
 * @property int $srv_status
 * @property string $srv_lng
 * @property int $srv_created_at
 * @property int $srv_updated_at
 *
 * @property User $srvUser
 */
class Services extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['srv_user_id', 'srv_image_id', 'srv_view_count', 'srv_like_count', 'srv_comment_count', 'srv_status', 'srv_created_at', 'srv_updated_at'], 'integer'],
            [['srv_body'], 'string'],
            [['srv_project_time_start', 'srv_project_time_end',],'safe'],
//            [['srv_created_at', 'srv_updated_at'], 'required'],
            [['srv_category'], 'string', 'max' => 20],
            [['srv_lng'],'string','max'=>3],
            [['srv_title'],'required'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1],
            [['srv_status'],'default','value'=>1],
            [['srv_user_id'],'default','value'=>Yii::$app->user->getId()],
            [['srv_created_at'],'default','value'=>time(),'on'=>'create'],
            [['srv_updated_at'],'default','value'=>time(),'on'=>'update'],
            [['srv_title', 'srv_location', 'srv_boss', 'srv_keyword', 'srv_meta_descriptions', 'srv_meta_title', 'srv_link'], 'string', 'max' => 255],
            [['srv_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['srv_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'srv_id' => Yii::t('app', 'Srv ID'),
            'srv_user_id' => Yii::t('app', 'Srv User ID'),
            'srv_category' => Yii::t('app', 'Srv Category'),
            'srv_title' => Yii::t('app', 'Srv Title'),
            'srv_body' => Yii::t('app', 'Srv Body'),
            'srv_image_id' => Yii::t('app', 'Srv Image ID'),
            'srv_location' => Yii::t('app', 'Srv Location'),
            'srv_project_time_start' => Yii::t('app', 'Srv Project Time Start'),
            'srv_project_time_end' => Yii::t('app', 'Srv Project Time End'),
            'srv_boss' => Yii::t('app', 'Srv Boss'),
            'srv_keyword' => Yii::t('app', 'Srv Keyword'),
            'srv_meta_descriptions' => Yii::t('app', 'Srv Meta Descriptions'),
            'srv_meta_title' => Yii::t('app', 'Srv Meta Title'),
            'sev_link' => Yii::t('app', 'Srv Link'),
            'srv_view_count' => Yii::t('app', 'Srv View Count'),
            'srv_like_count' => Yii::t('app', 'Srv Like Count'),
            'srv_comment_count' => Yii::t('app', 'Srv Comment Count'),
            'srv_status' => Yii::t('app', 'Srv Status'),
            'srv_created_at' => Yii::t('app', 'Srv Created At'),
            'srv_updated_at' => Yii::t('app', 'Srv Updated At'),
        ];
    }

    /**
     * Gets query for [[SrvUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSrvUser()
    {
        return $this->hasOne(User::className(), ['id' => 'srv_user_id']);
    }
}
