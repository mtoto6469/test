<?php


namespace common\models;

use Yii;
use yii\base\Model;
use common\models\User;

class Email extends Model
{
    public $email;

    /** {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
//                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     *  Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public static function sendEmail($email, $mess, $from, $to1, $subject1)
    {

        $user = User::findOne([
            'email' => $email,
        ]);

        if (!$user) {
            return false;
        }


        $to = $email;
        $subject = $subject1;
        $message = $mess;
        $headers = "From:" . $from;

        mail($to, $subject, $message, $headers);
        return true;
    }
}
