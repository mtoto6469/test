<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Categories;

/**
 * CategoriesSearch represents the model behind the search form of `common\models\Categories`.
 */
class CategoriesSearch extends Categories
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'cat_user_id', 'cat_parent_id', 'cat_image_id', 'cat_status', 'cat_created_at', 'cat_updated_at'], 'integer'],
            [['cat_type', 'cat_title', 'cat_descriptions', 'cat_en_title', 'cat_en_descriptions', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cat_id' => $this->cat_id,
            'cat_user_id' => $this->cat_user_id,
            'cat_parent_id' => $this->cat_parent_id,
            'cat_image_id' => $this->cat_image_id,
            'cat_status' => $this->cat_status,
            'cat_created_at' => $this->cat_created_at,
            'cat_updated_at' => $this->cat_updated_at,
        ]);

        $query->andFilterWhere(['like', 'cat_type', $this->cat_type])
            ->andFilterWhere(['like', 'cat_title', $this->cat_title])
            ->andFilterWhere(['like', 'cat_descriptions', $this->cat_descriptions])
            ->andFilterWhere(['like', 'cat_en_title', $this->cat_en_title])
            ->andFilterWhere(['like', 'cat_en_descriptions', $this->cat_en_descriptions])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
