<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tbl_files".
 *
 * @property int $img_id
 * @property int|null $img_user_id
 * @property string|null $img_type
 * @property int|null $img_type_id
 * @property string|null $img_enable
 * @property string|null $img_image
 * @property string|null $img_name
 * @property string|null $img_link
 * @property string $slug
 * @property int|null $img_view_count
 * @property int|null $img_like_count
 * @property int|null $img_comment_count
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_files';
    }
//    public function behaviors()
//    {
//        return[
//            'class'=>SluggableBehavior::className(),
//            'attribute'=>'img_image',
//            'ensureUnique'=>true
//        ];
//
//    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img_user_id', 'img_type_id', 'img_view_count', 'img_like_count', 'img_comment_count'], 'integer'],
//            [['slug'], 'required'],
            ['img_user_id','default','value'=>Yii::$app->user->getId()],
            ['img_enable','default','value'=>'1'],
            [['img_type'], 'string', 'max' => 10],
            [['img_enable', 'img_image', 'img_name', 'img_link', 'slug'], 'string', 'max' => 255],
            ['img_image','unique'],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'img_id' => Yii::t('app', 'Img ID'),
            'img_user_id' => Yii::t('app', 'Img User ID'),
            'img_type' => Yii::t('app', 'Img Type'),
            'img_type_id' => Yii::t('app', 'Img Type ID'),
            'img_enable' => Yii::t('app', 'Img Enable'),
            'img_image' => Yii::t('app', 'Img Image'),
            'img_name' => Yii::t('app', 'Img Name'),
            'img_link' => Yii::t('app', 'Img Link'),
            'slug' => Yii::t('app', 'Slug'),
            'img_view_count' => Yii::t('app', 'Img View Count'),
            'img_like_count' => Yii::t('app', 'Img Like Count'),
            'img_comment_count' => Yii::t('app', 'Img Comment Count'),
        ];
    }
}
