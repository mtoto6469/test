<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Services;

/**
 * ServicesSearch represents the model behind the search form of `common\models\Services`.
 */
class ServicesSearch extends Services
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['srv_id', 'srv_user_id', 'srv_image_id', 'srv_project_time_start', 'srv_project_time_end', 'srv_view_count', 'srv_like_count', 'srv_comment_count', 'srv_status', 'srv_created_at', 'srv_updated_at'], 'integer'],
            [['srv_category', 'srv_title', 'srv_body', 'srv_location', 'srv_boss', 'srv_keyword', 'srv_meta_descriptions', 'srv_meta_title', 'pro_link'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Services::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'srv_id' => $this->srv_id,
            'srv_user_id' => $this->srv_user_id,
            'srv_image_id' => $this->srv_image_id,
            'srv_project_time_start' => $this->srv_project_time_start,
            'srv_project_time_end' => $this->srv_project_time_end,
            'srv_view_count' => $this->srv_view_count,
            'srv_like_count' => $this->srv_like_count,
            'srv_comment_count' => $this->srv_comment_count,
            'srv_status' => $this->srv_status,
            'srv_created_at' => $this->srv_created_at,
            'srv_updated_at' => $this->srv_updated_at,
        ]);

        $query->andFilterWhere(['like', 'srv_category', $this->srv_category])
            ->andFilterWhere(['like', 'srv_title', $this->srv_title])
            ->andFilterWhere(['like', 'srv_body', $this->srv_body])
            ->andFilterWhere(['like', 'srv_location', $this->srv_location])
            ->andFilterWhere(['like', 'srv_boss', $this->srv_boss])
            ->andFilterWhere(['like', 'srv_keyword', $this->srv_keyword])
            ->andFilterWhere(['like', 'srv_meta_descriptions', $this->srv_meta_descriptions])
            ->andFilterWhere(['like', 'srv_meta_title', $this->srv_meta_title])
            ->andFilterWhere(['like', 'pro_link', $this->pro_link]);

        return $dataProvider;
    }
}
