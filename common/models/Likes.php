<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_likes".
 *
 * @property int $lik_id
 * @property int|null $lik_user_id
 * @property int|null $lik_computer_ip
 * @property int|null $lik_blag_id
 * @property int|null $lik_comment_id
 * @property int|null $lik_like
 * @property int $lik_created_at
 *
 * @property User $likUser
 */
class Likes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_likes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lik_user_id', 'lik_computer_ip', 'lik_blag_id', 'lik_comment_id', 'lik_like', 'lik_created_at'], 'integer'],
            [['lik_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['lik_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lik_id' => Yii::t('app', 'Lik ID'),
            'lik_user_id' => Yii::t('app', 'Lik User ID'),
            'lik_computer_ip' => Yii::t('app', 'Lik Computer Ip'),
            'lik_blag_id' => Yii::t('app', 'Lik Blag ID'),
            'lik_comment_id' => Yii::t('app', 'Lik Comment ID'),
            'lik_like' => Yii::t('app', 'Lik Like'),
            'lik_created_at' => Yii::t('app', 'Lik Created At'),
        ];
    }

    /**
     * Gets query for [[LikUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLikUser()
    {
        return $this->hasOne(User::className(), ['id' => 'lik_user_id']);
    }
}
