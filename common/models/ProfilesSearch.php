<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Profiles;

/**
 * ProfilesSearch represents the model behind the search form of `common\models\Profiles`.
 */
class ProfilesSearch extends Profiles
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pro_id', 'pro_user_id', 'pro_image_id', 'pro_valet', 'pro_updated_at'], 'integer'],
            [['pro_fullName', 'pro_position', 'pro_degree','pro_en_fullName', 'pro_en_position', 'pro_en_degree' ,'pro_birthday', 'pro_gender', 'pro_phone', 'pro_mobile', 'pro_nationalCode', 'pro_postalCode', 'pro_province', 'pro_city', 'pro_address', 'pro_descriptions', 'pro_biography','pro_en_address', 'pro_en_descriptions', 'pro_en_biography', 'pro_status', 'pro_created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Profiles::find()->where(['>','pro_user_id',2]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pro_id' => $this->pro_id,
            'pro_user_id' => $this->pro_user_id,
            'pro_image_id' => $this->pro_image_id,
            'pro_birthday' => $this->pro_birthday,
            'pro_valet' => $this->pro_valet,
            'pro_created_at' => $this->pro_created_at,
            'pro_updated_at' => $this->pro_updated_at,
        ]);

        $query->andFilterWhere(['like', 'pro_fullName', $this->pro_fullName])
            ->andFilterWhere(['like', 'pro_en_fullName', $this->pro_en_fullName])
            ->andFilterWhere(['like', 'pro_position', $this->pro_position])
            ->andFilterWhere(['like', 'pro_en_position', $this->pro_en_position])
            ->andFilterWhere(['like', 'pro_degree', $this->pro_degree])
            ->andFilterWhere(['like', 'pro_en_degree', $this->pro_en_degree])
            ->andFilterWhere(['like', 'pro_gender', $this->pro_gender])
            ->andFilterWhere(['like', 'pro_phone', $this->pro_phone])
            ->andFilterWhere(['like', 'pro_mobile', $this->pro_mobile])
            ->andFilterWhere(['like', 'pro_nationalCode', $this->pro_nationalCode])
            ->andFilterWhere(['like', 'pro_postalCode', $this->pro_postalCode])
            ->andFilterWhere(['like', 'pro_province', $this->pro_province])
            ->andFilterWhere(['like', 'pro_city', $this->pro_city])
            ->andFilterWhere(['like', 'pro_address', $this->pro_address])
            ->andFilterWhere(['like', 'pro_en_address', $this->pro_en_address])
            ->andFilterWhere(['like', 'pro_descriptions', $this->pro_descriptions])
            ->andFilterWhere(['like', 'pro_en_descriptions', $this->pro_en_descriptions])
            ->andFilterWhere(['like', 'pro_biography', $this->pro_biography])
            ->andFilterWhere(['like', 'pro_en_biography', $this->pro_en_biography])
            ->andFilterWhere(['like', 'pro_status', $this->pro_status]);

        return $dataProvider;
    }
}
