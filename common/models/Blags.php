<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_blags".
 *
 * @property int $blg_id
 * @property int $blg_user_id
 * @property int $blg_category_id
 * @property int $blg_image_id
 * @property string $blg_title
 * @property string|null $blg_body
 * @property string $blg_descriptions
 * @property string|null $blg_laid
 * @property string|null $blg_keyword
 * @property string|null $blg_meta_descriptions
 * @property string|null $blg_meta_title
 * @property string|null $blg_link
 * @property string $slug
 * @property string|null $blg_writer
 * @property int|null $blg_view_count
 * @property int|null $blg_like_count
 * @property int|null $blg_comment_count
 * @property int $blg_status
 * @property int $blg_created_at
 * @property int $blg_updated_at
 *
 * @property User $blgUser
 */
class Blags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_blags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blg_user_id', 'blg_category_id', 'blg_image_id', 'blg_title', 'blg_descriptions', 'slug', 'blg_created_at', 'blg_updated_at'], 'required'],
            [['blg_user_id', 'blg_category_id', 'blg_image_id', 'blg_view_count', 'blg_like_count', 'blg_comment_count', 'blg_status', 'blg_created_at', 'blg_updated_at'], 'integer'],
            [['blg_body'], 'string'],
            [['blg_title', 'blg_descriptions', 'blg_laid', 'blg_keyword', 'blg_meta_descriptions', 'blg_meta_title', 'blg_link', 'slug', 'blg_writer'], 'string', 'max' => 255],
            [['blg_title'], 'unique'],
            [['slug'], 'unique'],
            [['blg_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['blg_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'blg_id' => Yii::t('app', 'Blg ID'),
            'blg_user_id' => Yii::t('app', 'Blg User ID'),
            'blg_category_id' => Yii::t('app', 'Blg Category ID'),
            'blg_image_id' => Yii::t('app', 'Blg Image ID'),
            'blg_title' => Yii::t('app', 'Blg Title'),
            'blg_body' => Yii::t('app', 'Blg Body'),
            'blg_descriptions' => Yii::t('app', 'Blg Descriptions'),
            'blg_laid' => Yii::t('app', 'Blg Laid'),
            'blg_keyword' => Yii::t('app', 'Blg Keyword'),
            'blg_meta_descriptions' => Yii::t('app', 'Blg Meta Descriptions'),
            'blg_meta_title' => Yii::t('app', 'Blg Meta Title'),
            'blg_link' => Yii::t('app', 'Blg Link'),
            'slug' => Yii::t('app', 'Slug'),
            'blg_writer' => Yii::t('app', 'Blg Writer'),
            'blg_view_count' => Yii::t('app', 'Blg View Count'),
            'blg_like_count' => Yii::t('app', 'Blg Like Count'),
            'blg_comment_count' => Yii::t('app', 'Blg Comment Count'),
            'blg_status' => Yii::t('app', 'Blg Status'),
            'blg_created_at' => Yii::t('app', 'Blg Created At'),
            'blg_updated_at' => Yii::t('app', 'Blg Updated At'),
        ];
    }

    /**
     * Gets query for [[BlgUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBlgUser()
    {
        return $this->hasOne(User::className(), ['id' => 'blg_user_id']);
    }
    public function getBlgCategory()
    {
        return $this->hasOne(Categories::className(), ['cat_id' => 'blg_category_id']);
    }
}
