<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $key
 * @property string $title
 * @property string|null $body
 */
class Pages extends \yii\db\ActiveRecord
{
    public $logo;
    public $en_logo;
    public $labelTitle;
    public $en_labelTitle;
    public $seo_title;
    public $en_seo_title;
    public $phone;
    public $fax;
    public $email;
    public $description;
    public $en_description;
    public $seo_description;
    public $en_seo_description;
    public $about;
    public $en_about;
    public $rule;
    public $en_rule;
    public $telegram;
    public $instagram;
    public $address;
    public $en_address;
    public $key_word;
    public $en_key_word;
    public $strategic_program;
    public $en_strategic_program;
    public $chart;//image
    public $en_chart;//image

    const ITEM_LOGO=1;
    const ITEM_EN_LOGO=2;
    const ITEM_LABEL_TITLE=3;
    const ITEM_EN_LABEL_TITLE=4;
    const ITEM_SEO_TITLE=5;
    const ITEM_EN_SEO_TITLE=6;
    const ITEM_TELEPHONE=7;
    const ITEM_EMAIL=8;
    const ITEM_DESCRIPTION=9;
    const ITEM_EN_DESCRIPTION=10;
    const ITEM_SEO_DESCRIPTION=11;
    const ITEM_EN_SEO_DESCRIPTION=12;
    const ITEM_ADDRESS=13;
    const ITEM_EN_ADDRESS=14;
    const ITEM_TELEGRAM=15;
    const ITEM_INSTEGERAM=16;
    const ITEM_ABOUT=17;
    const ITEM_EN_ABOUT=18;
    const ITEM_RULE=19;
    const ITEM_EN_RULE=20;
    const ITEM_KEYWORD=21;
    const ITEM_EN_KEYWORD=22;
    const ITEM_STRATEGIC_PROGRAM=23;
    const ITEM_EN_STRATEGIC_PROGRAM=24;
    const ITEM_CHART=25;
    const ITEM_EN_CHART=26;
    const ITEM_FAX=27;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['user_id'], 'integer'],
//            [['key', 'title'], 'required'],
//            [['body'], 'string'],
//            [['key', 'title'], 'string', 'max' => 255],
            [['title','about','rule','key_word','en_about','en_rule','en_key_word','strategic_program','en_strategic_program'], 'string'],
            [['key','phone'], 'integer'],
//            [['address_code'], 'safe'],
            //[['latitude','longitude','instageram','telegram'],'safe'],
            [['instagram','telegram'],'safe'],

            [['labelTitle'], 'string','min'=>'3', 'max' => 50],
            [['en_labelTitle'], 'string','min'=>'3', 'max' => 50],
            [['seo_title','en_seo_title'], 'string','min'=>'3', 'max' => 50],
            [['seo_description','en_seo_description'], 'string','min'=>'3', 'max' => 255],
            [['address','en_address'], 'string','min'=>'3', 'max' => 120],
            [['description','rule','en_description','en_rule'], 'string', 'max' => 500],
            [['logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1],
            [['en_logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1],
            [['chart'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1],
            [['en_chart'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1],
            [['phone'], 'integer','min'=>11],
            ['phone','match','pattern'=>'/^0+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]/'],
            ['fax','match','pattern'=>'/^0+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]/'],
            ['email', 'trim'],
            ['email', 'email'],
            [['key'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'key' => Yii::t('app', 'Key'),
            'title' => Yii::t('app', 'Title'),
            'body' => Yii::t('app', 'Body'),
        ];
    }
}
