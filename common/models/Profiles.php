<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_profiles".
 *
 * @property int $pro_id
 * @property int|null $pro_user_id
 * @property string|null $pro_fullName
 * @property string|null $pro_en_fullName
 * @property int|null $pro_image_id
 * @property string|null $pro_position
 * @property string|null $pro_en_position
 * @property string|null $pro_degree
 * @property string|null $pro_en_degree
 * @property string|null $pro_birthday
 * @property string|null $pro_gender
 * @property string|null $pro_phone
 * @property string|null $pro_mobile
 * @property string|null $pro_nationalCode
 * @property string|null $pro_postalCode
 * @property string|null $pro_province
 * @property string|null $pro_city
 * @property string|null $pro_address
 * @property string|null $pro_en_address
 * @property string|null $pro_descriptions
 * @property string|null $pro_en_descriptions
 * @property string|null $pro_biography
 * @property string|null $pro_en_biography
 * @property int|null $pro_valet
 * @property string|null $pro_status
 * @property string $pro_created_at
 * @property int|null $pro_updated_at
 *
 * @property User $proUser
 */
class Profiles extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_profiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pro_user_id', 'pro_image_id', 'pro_valet', 'pro_updated_at'], 'integer'],
            [['pro_birthday', 'pro_created_at'], 'safe'],
            [['pro_biography'], 'string'],
            [['pro_fullName','pro_en_fullName'], 'string', 'max' => 50],
            [['pro_position', 'pro_degree','pro_en_position', 'pro_en_degree', 'pro_city'], 'string', 'max' => 30],
            [['pro_gender', 'pro_status'], 'string', 'max' => 1],
            [['pro_phone', 'pro_mobile', 'pro_nationalCode', 'pro_postalCode'], 'string', 'max' => 11],
            [['pro_province'], 'string', 'max' => 20],
            [['pro_address','pro_en_address'], 'string', 'max' => 200],
            [['pro_descriptions','pro_en_descriptions'], 'string', 'max' => 255],
            [['pro_mobile'],'string','min'=>11,'max'=>11],
            [['pro_mobile'],'match' ,'pattern' => '/^(\+98|0)?9\d{9}$/'],
            [['pro_phone'],'match' ,'pattern' => '/^[0-9]{11}$/'],
            [['pro_postalCode'],'match' ,'pattern' => '/^[0-9]{10}$/'],
            [['pro_nationalCode'],'match', 'pattern' => '/^[0-9]{10}$/'],
//            [['pro_nationalCode'],Yii::$app->nationalCode->NationalCode()],
            [['pro_created_at'],'default','value'=>time(),'on'=>'create'],
            [['pro_updated_at'],'default','value'=>time(),'on'=>'update'],
            [['image'],'file'],
            [['pro_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['pro_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pro_id' => Yii::t('app', 'Pro ID'),
            'pro_user_id' => Yii::t('app', 'Pro User ID'),
            'pro_fullName' => Yii::t('app', 'Pro Full Name'),
            'pro_en_fullName' => Yii::t('app', 'Pro En Full Name'),
            'pro_image_id' => Yii::t('app', 'Pro Image ID'),
            'pro_position' => Yii::t('app', 'Pro Position'),
            'pro_en_position' => Yii::t('app', 'Pro En Position'),
            'pro_degree' => Yii::t('app', 'Pro Degree'),
            'pro_en_degree' => Yii::t('app', 'Pro En Degree'),
            'pro_birthday' => Yii::t('app', 'Pro Birthday'),
            'pro_en_birthday' => Yii::t('app', 'Pro En Birthday'),
            'pro_gender' => Yii::t('app', 'Pro Gender'),
            'pro_phone' => Yii::t('app', 'Pro Phone'),
            'pro_mobile' => Yii::t('app', 'Pro Mobile'),
            'pro_nationalCode' => Yii::t('app', 'Pro National Code'),
            'pro_postalCode' => Yii::t('app', 'Pro Postal Code'),
            'pro_province' => Yii::t('app', 'Pro Province'),
            'pro_city' => Yii::t('app', 'Pro City'),
            'pro_address' => Yii::t('app', 'Pro Address'),
            'pro_en_address' => Yii::t('app', 'Pro En Address'),
            'pro_descriptions' => Yii::t('app', 'Pro Descriptions'),
            'pro_en_descriptions' => Yii::t('app', 'Pro En Descriptions'),
            'pro_biography' => Yii::t('app', 'Pro Biography'),
            'pro_en_biography' => Yii::t('app', 'Pro En Biography'),
            'pro_valet' => Yii::t('app', 'Pro Valet'),
            'pro_status' => Yii::t('app', 'Pro Status'),
            'pro_created_at' => Yii::t('app', 'Pro Created At'),
            'pro_updated_at' => Yii::t('app', 'Pro Updated At'),
        ];
    }

    /**
     * Gets query for [[ProUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProUser()
    {
        return $this->hasOne(User::className(), ['id' => 'pro_user_id']);
    }

    public function nationalCode($nationalCode)
    {
        //چک کردن خالی بودن کد
        if ($nationalCode != null) {
            $numMeliCode = strlen($nationalCode);//به دست آوردن تعداد
            if ($numMeliCode == 10) {
                if ($nationalCode == '0000000000' ||
                    $nationalCode == '1111111111' ||
                    $nationalCode == '2222222222' ||
                    $nationalCode == '3333333333' ||
                    $nationalCode == '4444444444' ||
                    $nationalCode == '5555555555' ||
                    $nationalCode == '6666666666' ||
                    $nationalCode == '7777777777' ||
                    $nationalCode == '8888888888' ||
                    $nationalCode == '9999999999'
                ) {
                    $send['result'] = 400;
                    $send['message'] = 'لطفاارقام کد ملی را درست وارد کنید';
                    return $send;
                } else {
                    $sum = 0;
                    $x = 10;
                    //جمع کردن تمام ارقام کد ملی
                    for ($i = 0; $i <= 8; $i++) {
                        $sum = (int)$sum + ((int)$nationalCode[$i] * (int)$x);
                        $x = $x - 1;
                    }//end for i
                    $mod = $sum % 11;
                    if ($mod >= 2) {
                        $control = 11 - $mod;
                        if ($control == $nationalCode[9]) {
                            $send['result'] = 200;
                            $send['message'] = ' کد ملی صحیح میباشد';
                            $send['nationalCode'] = $nationalCode;
                            return $send;
                        }//end if control
                        else {
                            $send['result'] = 400;
                            $send['message'] = ' کد ملی صحیح نمیباشد';
                            return $send;
                        }//end else control
                    }//end if mod
                    else {
                        if ($mod == $nationalCode[9]) {
                            $send['result'] = 200;
                            $send['message'] = ' کد ملی صحیح میباشد';
                            $send['nationalCode'] = $nationalCode;
                            return $send;
                        } else {
                            $send['result'] = 400;
                            $send['message'] = ' کد ملی صحیح نمیباشد';
                            return $send;
                        }//end else mod

                    }//end else mode
                }
            }//end if $numMeliCode == 10
            else {
                $send['result'] = 400;
                $send['message'] = 'لطفاتعداد ارقام کد ملی را درست وارد کنید';
                return $send;
            }//end else $numMeliCode != 10
        }//چک کردن داده ها
        else {
            $send['result'] = 400;
            $send['message'] = 'لطفا کد ملی را وارد کنید';
            return $send;
        }//چک کردن داده ها
    }
}
