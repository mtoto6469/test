<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_categories".
 *
 * @property int $cat_id
 * @property int $cat_user_id
 * @property int|null $cat_parent_id
 * @property int $cat_image_id
 * @property string $cat_type
 * @property string $cat_title
 * @property string|null $cat_descriptions
 * @property string|null $cat_en_title
 * @property string|null $cat_en_descriptions
 * @property int $cat_status
 * @property string $slug
 * @property int $cat_created_at
 * @property int $cat_updated_at
 *
 * @property User $catUser
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_user_id', 'cat_image_id', 'cat_type', 'cat_title', 'slug', 'cat_created_at', 'cat_updated_at'], 'required'],
            [['cat_user_id', 'cat_parent_id', 'cat_image_id', 'cat_status', 'cat_created_at', 'cat_updated_at'], 'integer'],
            [['cat_type'], 'string', 'max' => 1],
            [['cat_title', 'cat_descriptions', 'cat_en_title', 'cat_en_descriptions', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['cat_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['cat_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cat_id' => Yii::t('app', 'Cat ID'),
            'cat_user_id' => Yii::t('app', 'Cat User ID'),
            'cat_parent_id' => Yii::t('app', 'Cat Parent ID'),
            'cat_image_id' => Yii::t('app', 'Cat Image ID'),
            'cat_type' => Yii::t('app', 'Cat Type'),
            'cat_title' => Yii::t('app', 'Cat Title'),
            'cat_descriptions' => Yii::t('app', 'Cat Descriptions'),
            'cat_en_title' => Yii::t('app', 'Cat En Title'),
            'cat_en_descriptions' => Yii::t('app', 'Cat En Descriptions'),
            'cat_status' => Yii::t('app', 'Cat Status'),
            'slug' => Yii::t('app', 'Slug'),
            'cat_created_at' => Yii::t('app', 'Cat Created At'),
            'cat_updated_at' => Yii::t('app', 'Cat Updated At'),
        ];
    }

    /**
     * Gets query for [[CatUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatUser()
    {
        return $this->hasOne(User::className(), ['id' => 'cat_user_id']);
    }
}
