<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_links".
 *
 * @property int $lnk_id
 * @property int|null $lnk_user_id
 * @property string $lnk_title
 * @property string $lnk_link
 * @property string|null $lnk_lng
 * @property int $lnk_created_at
 *
 * @property User $lnkUser
 */
class Links extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lnk_user_id', 'lnk_created_at'], 'integer'],
            [['lnk_title','lnk_link'], 'required'],
            [['lnk_title','lnk_link'], 'unique'],
            [['lnk_title','lnk_link'], 'string', 'max' => 255],
            [['lnk_lng'], 'string', 'max' => 4],
            [['lnk_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['lnk_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lnk_id' => Yii::t('app', 'Lnk ID'),
            'lnk_user_id' => Yii::t('app', 'Lnk User ID'),
            'lnk_title' => Yii::t('app', 'Lnk Title'),
            'lnk_link' => Yii::t('app', 'Lnk Link'),
            'lnk_lng' => Yii::t('app', 'Lnk Lng'),
            'lnk_created_at' => Yii::t('app', 'Lnk Created At'),
        ];
    }

    /**
     * Gets query for [[LnkUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLnkUser()
    {
        return $this->hasOne(User::className(), ['id' => 'lnk_user_id']);
    }
}
