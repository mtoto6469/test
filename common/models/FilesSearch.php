<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Files;

/**
 * FilesSearch represents the model behind the search form of `common\models\Files`.
 */
class FilesSearch extends Files
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img_id', 'img_user_id', 'img_type_id', 'img_view_count', 'img_like_count', 'img_comment_count'], 'integer'],
            [['img_type', 'img_enable', 'img_image', 'img_name', 'img_link', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$s=null)
    {
        $query = Files::find();

        if ($s){
            $query->filterWhere(['img_type'=>$s]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'img_id' => $this->img_id,
            'img_user_id' => $this->img_user_id,
            'img_type_id' => $this->img_type_id,
            'img_view_count' => $this->img_view_count,
            'img_like_count' => $this->img_like_count,
            'img_comment_count' => $this->img_comment_count,
        ]);

        $query->andFilterWhere(['like', 'img_type', $this->img_type])
            ->andFilterWhere(['like', 'img_enable', $this->img_enable])
            ->andFilterWhere(['like', 'img_image', $this->img_image])
            ->andFilterWhere(['like', 'img_name', $this->img_name])
            ->andFilterWhere(['like', 'img_link', $this->img_link])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
