<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Blags;

/**
 * BlagsSearch represents the model behind the search form of `common\models\Blags`.
 */
class BlagsSearch extends Blags
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blg_id', 'blg_user_id', 'blg_category_id', 'blg_image_id', 'blg_view_count', 'blg_like_count', 'blg_comment_count', 'blg_status', 'blg_created_at', 'blg_updated_at'], 'integer'],
            [['blg_title', 'blg_body', 'blg_descriptions', 'blg_laid', 'blg_keyword', 'blg_meta_descriptions', 'blg_meta_title', 'blg_link', 'slug', 'blg_writer'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Blags::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'blg_id' => $this->blg_id,
            'blg_user_id' => $this->blg_user_id,
            'blg_category_id' => $this->blg_category_id,
            'blg_image_id' => $this->blg_image_id,
            'blg_view_count' => $this->blg_view_count,
            'blg_like_count' => $this->blg_like_count,
            'blg_comment_count' => $this->blg_comment_count,
            'blg_status' => $this->blg_status,
            'blg_created_at' => $this->blg_created_at,
            'blg_updated_at' => $this->blg_updated_at,
        ]);

        $query->andFilterWhere(['like', 'blg_title', $this->blg_title])
            ->andFilterWhere(['like', 'blg_body', $this->blg_body])
            ->andFilterWhere(['like', 'blg_descriptions', $this->blg_descriptions])
            ->andFilterWhere(['like', 'blg_laid', $this->blg_laid])
            ->andFilterWhere(['like', 'blg_keyword', $this->blg_keyword])
            ->andFilterWhere(['like', 'blg_meta_descriptions', $this->blg_meta_descriptions])
            ->andFilterWhere(['like', 'blg_meta_title', $this->blg_meta_title])
            ->andFilterWhere(['like', 'blg_link', $this->blg_link])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'blg_writer', $this->blg_writer]);

        return $dataProvider;
    }
}
