<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Likes;

/**
 * LikesSearch represents the model behind the search form of `common\models\Likes`.
 */
class LikesSearch extends Likes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lik_id', 'lik_user_id', 'lik_computer_ip', 'lik_blag_id', 'lik_comment_id', 'lik_like', 'lik_created_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Likes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lik_id' => $this->lik_id,
            'lik_user_id' => $this->lik_user_id,
            'lik_computer_ip' => $this->lik_computer_ip,
            'lik_blag_id' => $this->lik_blag_id,
            'lik_comment_id' => $this->lik_comment_id,
            'lik_like' => $this->lik_like,
            'lik_created_at' => $this->lik_created_at,
        ]);

        return $dataProvider;
    }
}
