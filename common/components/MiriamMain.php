<?php


namespace common\components;

use yii;
use common\models\Files;

class MiriamMain extends yii\base\Component
{
    public  function ImageUpload($img,$type,$name=null,$path=false)
    {

        $file=new Files();
        if ($img){
            if ($name==null){
                $name='miriam_'.$img->baseName.'_'.time();
            }
            $name=$name.'.'.$img->extension;
            if ($path){
//                echo Yii::getAlias('@upload').'/users/'.$name.'\n';
//                echo Yii::getAlias('@upload').'/'.$path.'/'.$name;exit;
                $img->saveAs(Yii::getAlias('@upload').'/'.$path.'/'.$name);
            }else{
                $img->saveAs(Yii::getAlias('@upload').'/'.$name);
            }
            $file->img_image=$name;
            $file->slug=$name;
            $file->img_type=$type;
            if (!$file->save()){
                var_dump($file->errors);exit;
            }
            $send['image_id']=$file->img_id;
            $send['result']=true;
            return $send;
        }
        $send['result']=false;
        return $send;
    }

    public  function FileExist($name,$path=false)
    {
        if ($path){
            if (file_exists(Yii::getAlias('@upload').'/'.$path.'/'.$name)){
                return true;
            }
            return false;
        }else{
            if (file_exists(Yii::getAlias('@upload').'/'.$name)){
                return true;
            }
            return false;
        }
    }

    public  function ShowImage($image,$path=false){
        $image_id=Files::findOne($image);
        if (!$image_id) {
            return false;
        }
            if ($path) {
                return Yii::$app->urlManager->hostInfo . '/upload/'.$path.'/' . (strlen($image_id->img_image) > 3 && MiriamMain::FileExist($image_id->img_image,$path) ? $image_id->img_image : '');
            } else {
                return Yii::$app->urlManager->hostInfo . '/upload/' . (strlen($image_id->img_image) > 3 && MiriamMain::FileExist($image_id->img_image) ? $image_id->img_image : '');
            }

    }

    //gender
    public static function gender($gender)
    {
     if ($gender==1){
         return Yii::t('app','female');
     }else{
         return Yii::t('app','male');
     }
    }

}