<?php
namespace common\components;

use yii;
use yii\web\Cookie;
//for multilingual
class Controller extends \yii\web\Controller
{
    public function init(){
        if(isset($_GET['language']))
        {
//            echo $_GET['language'];
            Yii::$app->language=$_GET['language'];
            Yii::$app->session->set('language',$_GET['language']);

            $cookie=new Cookie([
                'name'=>'language',
                'value'=>$_GET['language'],
            ]);
            $cookie->expire=time()+(60*60*24*365);
            Yii::$app->response->cookies->add($cookie);
        }elseif (Yii::$app->session->has('language'))
        {
            Yii::$app->language=Yii::$app->session->get('language');
        }elseif (Yii::$app->request->cookies['language'])
        {
            Yii::$app->language=Yii::$app->request->cookies['language']->value;
        }
        parent::init();
    }
}
