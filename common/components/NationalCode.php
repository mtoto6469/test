<?php


namespace common\components;


use yii\validators\Validator;

class NationalCode extends Validator
{
    public function nationalCode($nationalCode)
    {

        //چک کردن خالی بودن کد
        if ($nationalCode != null) {
            $numMeliCode = strlen($nationalCode);//به دست آوردن تعداد
            if ($numMeliCode == 10) {
                if ($nationalCode == '0000000000' ||
                    $nationalCode == '1111111111' ||
                    $nationalCode == '2222222222' ||
                    $nationalCode == '3333333333' ||
                    $nationalCode == '4444444444' ||
                    $nationalCode == '5555555555' ||
                    $nationalCode == '6666666666' ||
                    $nationalCode == '7777777777' ||
                    $nationalCode == '8888888888' ||
                    $nationalCode == '9999999999'
                ) {
                    $send['result'] = 400;
                    $send['message'] = 'لطفاارقام کد ملی را درست وارد کنید';
                    return $send;
                } else {
                    $sum = 0;
                    $x = 10;
                    //جمع کردن تمام ارقام کد ملی
                    for ($i = 0; $i <= 8; $i++) {
                        $sum = (int)$sum + ((int)$nationalCode[$i] * (int)$x);
                        $x = $x - 1;
                    }//end for i
                    $mod = $sum % 11;
                    if ($mod >= 2) {
                        $control = 11 - $mod;
                        if ($control == $nationalCode[9]) {
                            $send['result'] = 200;
                            $send['message'] = ' کد ملی صحیح میباشد';
                            $send['nationalCode'] = $nationalCode;
                            return $send;
                        }//end if control
                        else {
                            $send['result'] = 400;
                            $send['message'] = ' کد ملی صحیح نمیباشد';
                            return $send;
                        }//end else control
                    }//end if mod
                    else {
                        if ($mod == $nationalCode[9]) {
                            $send['result'] = 200;
                            $send['message'] = ' کد ملی صحیح میباشد';
                            $send['nationalCode'] = $nationalCode;
                            return $send;
                        } else {
                            $send['result'] = 400;
                            $send['message'] = ' کد ملی صحیح نمیباشد';
                            return $send;
                        }//end else mod

                    }//end else mode
                }
            }//end if $numMeliCode == 10
            else {
                $send['result'] = 400;
                $send['message'] = 'لطفاتعداد ارقام کد ملی را درست وارد کنید';
                return $send;
            }//end else $numMeliCode != 10
        }//چک کردن داده ها
        else {
            $send['result'] = 400;
            $send['message'] = 'لطفا کد ملی را وارد کنید';
            return $send;
        }//چک کردن داده ها
    }
}