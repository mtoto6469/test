<?php
namespace common\components;

use yii\base\Component;
use yii\helpers\Html;

class MessageComponent extends Component
{

    public $content;

    public function init()
    {
        parent::init();
        $this->content='miriam yii2 test';
    }

    public function display($content=null)
    {
        if ($content!= null){
            $this->content=$content;
        }
        echo Html::encode($this->content);
    }
}
